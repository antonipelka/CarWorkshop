import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Part } from 'parts/part.entity';
import { Repository } from 'typeorm';
import { Service } from './service.entity';
import { CreateServiceDto } from './dtos/create-Service.dto';
import { UpdateServiceDto } from './dtos/update-Service.dto';
import { AuthService } from 'auth/auth.service';

@Injectable()
export class ServicesService {
    constructor(
        @InjectRepository(Service) private readonly serviceRepository: Repository<Service>,
        @InjectRepository(Part) private readonly partRepository: Repository<Part>,
        private readonly authService: AuthService,
    ) { }

    async findAll(userId: number, returnAll: boolean): Promise<Service[]> {
        const user = await this.authService.getAuthenticatedUserById(userId);

        return await this.serviceRepository.find({ where: user.isEmployee || returnAll ? {} : { isChoosable: true }, relations: ['parts'] });
    }

    async create({
        name,
        isChoosable,
        description,
        price,
        partIds,
    }: CreateServiceDto): Promise<Service> {
        const parts = partIds.map(id => {
            return { id };
        });

        const service = this.serviceRepository.create({
            name,
            isChoosable,
            description,
            price,
            parts,
        });

        const persistedService = await this.serviceRepository.save(service);
        return this.getServiceById(persistedService.id, true);
    }

    async remove(id: number) {
        return await this.serviceRepository.delete(id);
    }

    async update(
        id: number,
        updateServiceDto: UpdateServiceDto,
    ): Promise<Service> {
        const service = await this.getServiceById(id);
        const parts = await this.partRepository.findByIds(updateServiceDto.partIds);
        await this.serviceRepository.save({ ...service, ...updateServiceDto, parts });

        return await this.getServiceById(id, true);
    }

    async getServiceById(id: number, withRelations = false) {
        return await this.serviceRepository.findOne(id, { relations: withRelations ? ['parts'] : [] });
    }
}
