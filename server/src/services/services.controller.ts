import { Body, Controller, Delete, Get, Param, Post, Put, Req, UseGuards, ValidationPipe, Query } from '@nestjs/common';
import { EmployeeGuard } from '../shared/authorization/employee.guard';
import { CreateServiceDto } from './dtos/create-service.dto';
import { UpdateServiceDto } from './dtos/update-service.dto';
import { ServicesService } from './services.service';

@Controller('services')
export class ServicesController {
    constructor(private readonly servicesService: ServicesService) { }

    @Get()
    index(@Req() req, @Query('all') returnAll) {
        return this.servicesService.findAll(req.user.id, returnAll);
    }

    @Post()
    @UseGuards(EmployeeGuard)
    async store(@Req() req,  @Body(new ValidationPipe({ validationError: { target: false, value: false } })) createServiceDto: CreateServiceDto) {
        return this.servicesService.create(createServiceDto);
    }

    @Delete(':id')
    @UseGuards(EmployeeGuard)
    async remove(@Param() {id}){
       return this.servicesService.remove(id);
    }

    @Put(':id')
    @UseGuards(EmployeeGuard)
    async update(@Param() params, @Body() updateOrderDto: UpdateServiceDto){
        return this.servicesService.update(params.id , updateOrderDto);
    }
}
