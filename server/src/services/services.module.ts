import { Module } from '@nestjs/common';
import { ServicesController } from './services.controller';
import { ServicesService } from './services.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Service } from './service.entity';
import { AuthModule } from 'auth/auth.module';
import { Part } from 'parts/part.entity';

@Module({
    imports: [
        AuthModule,
        TypeOrmModule.forFeature([Service, Part]),
    ],
    controllers: [ServicesController],
    providers: [ServicesService],
})
export class ServicesModule { }
