import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { OrderService } from '../order-service/order-service.entity';
import { Part } from '../parts/part.entity';

@Entity({
    name: 'services',
})
export class Service {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({
        default: 0,
    })
    price: number;

    @Column({
        nullable: true,
    })
    description: string;

    @Column({
        nullable: false,
        default: false,
    })
    isChoosable: boolean;

    @OneToMany(type => OrderService, orderServices => orderServices.service)
    orderServices: OrderService[];

    @ManyToMany(type => Part, parts => parts.services)
    @JoinTable({name: 'servicesParts'})
    parts: Part[];
}
