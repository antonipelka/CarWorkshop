import { IsEmail, MinLength, IsString, IsBoolean, IsOptional, IsAlpha, IsNumberString, ValidateIf, IsNotEmpty } from 'class-validator';
import { Timestamp } from 'typeorm';
import { OrderService } from '../../order-service/order-service.entity';
import { Part } from '../../parts/part.entity';

export class UpdateServiceDto {

    @IsString()
    name: string;

    price: number;

    @IsString()
    description: string;

    @IsBoolean()
    isChoosable: boolean;

    partIds: number[];

}