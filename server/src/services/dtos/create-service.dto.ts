import { IsString } from 'class-validator';

export class CreateServiceDto {
    @IsString()
    readonly name: string;
    readonly price: number = 0;
    readonly description: string;
    readonly isChoosable: boolean = false;
    readonly partIds: number[] = [];
}
