import { Module } from '@nestjs/common';
import { CarsController } from './cars.controller';
import { CarsService } from './cars.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Car } from './car.entity';
import { AuthModule } from 'auth/auth.module';
import { User } from 'users/user.entity';
import { Order } from 'orders/order.entity';

@Module({
    imports: [
        AuthModule,
        TypeOrmModule.forFeature([Car, User, Order]),
    ],
    controllers: [CarsController],
    providers: [CarsService],
})
export class CarsModule { }
