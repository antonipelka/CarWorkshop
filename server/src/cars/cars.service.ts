import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../users/user.entity';
import { Car } from './car.entity';
import { CreateCarDto } from './dtos/create-car.dto';
import { UpdateCarDto } from './dtos/update-car.dto';
import { Order } from 'orders/order.entity';

@Injectable()
export class CarsService {

    constructor(
        @InjectRepository(Car) private readonly carRepository: Repository<Car>,
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        @InjectRepository(Order) private readonly orderRepository: Repository<Order>,
    ) { }

    async findAll(userId: number): Promise<Car[]> {
        const user = await this.userRepository.findOne(userId);
        return await user.isEmployee
            ? this.carRepository.find()
            : this.getCarsByClient(userId);
    }

    async create(
        {
            name,
            userId,
        }: CreateCarDto,
        authenticatedUserId: number = null,
    ): Promise<Car> {
        let user = await this.userRepository.findOne(authenticatedUserId);
        if (user.isEmployee) {
            user = await this.userRepository.findOne(userId);
        }
        const car = this.carRepository.create({ name, user });

        return await this.carRepository.save(car);
    }

    async remove(id: number) {
        return await this.carRepository.delete(id);
    }

    async update(id: number, object: UpdateCarDto): Promise<Car> {
        await this.carRepository.update(id, object);

        return await this.getCarById(id);
    }

    async getCarById(id: number) {
        return await this.carRepository.findOne(id);
    }

    async getCarsByClient(id: number) {
        return await this.carRepository.find({ where: { user: +id } });
    }

    async getCarsByOrderClient(orderId: number) {
        const order = await this.orderRepository.findOne(orderId, {
            relations: ['client'],
        });

        return await this.carRepository.find({
            where: {
                user: order.client.id,
            },
        });
    }
}
