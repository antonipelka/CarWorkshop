import { Controller, Get, Post, Body, ValidationPipe, Delete, Patch, Put, Param, Req, UseGuards, Query } from '@nestjs/common';
import { CarsService } from './cars.service';
import { CreateCarDto } from './dtos/create-car.dto';
import { UpdateCarDto } from './dtos/update-car.dto';

@Controller('cars')
export class CarsController {
    constructor(private readonly carService: CarsService) { }

    @Get()
    index(@Req() request, @Query('clientId') clientId, @Query('orderId') orderId) {
        return orderId
            ? this.carService.getCarsByOrderClient(orderId)
            : clientId
                ? this.carService.getCarsByClient(clientId)
                : this.carService.findAll(request.user.id);
    }

    @Post()
    async store(@Body(new ValidationPipe({ validationError: { target: false, value: false } })) createCarDto: CreateCarDto, @Req() req) {
        return this.carService.create(createCarDto, req.user.id);
    }

    @Delete(':id')
    async remove(@Param() { id }) {
        return this.carService.remove(id);
    }

    @Put(':id')
    async update(@Param('id') id, @Body() updateCarDto: UpdateCarDto) {
        return this.carService.update(id, updateCarDto);
    }
}
