import { IsString } from 'class-validator';

export class UpdateCarDto {
    @IsString()
    name: string;
}
