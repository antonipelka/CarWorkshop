import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Order } from '../orders/order.entity';
import { Service } from '../services/service.entity';

@Entity({
    name: 'orderService',
})
export class OrderService {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        nullable: false,
    })
    isCompleted: boolean;

    @ManyToOne(type => Order, order => order.orderServices, { onDelete: 'CASCADE' })
    order: Order;

    @ManyToOne(type => Service, service => service.orderServices, { onDelete: 'CASCADE' })
    service: Service;
}
