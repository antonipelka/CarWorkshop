import { IsEmail, MinLength, IsString, IsBoolean, IsOptional, IsAlpha, IsNumberString, ValidateIf, IsNotEmpty } from "class-validator";
import { EqualsTo } from "shared/validation/equals-to";

export class UpdateOrderServiceDto {
    @IsBoolean()
    isCompleted: boolean;
 
}