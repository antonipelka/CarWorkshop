import { IsEmail, MinLength, IsString, IsBoolean, IsOptional, IsAlpha, IsNumberString, ValidateIf, IsNotEmpty } from "class-validator";
import { EqualsTo } from "shared/validation/equals-to";

export class CreateOrderServiceDto {

    orderId: number;

    serviceId: number;

    @IsBoolean()
    isCompleted: boolean;
 
}