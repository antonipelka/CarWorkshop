import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { OrderService } from './order-service.entity';

@Injectable()
export class OrdersServicesService {
    constructor(@InjectRepository(OrderService) private readonly orderServiceRepository: Repository<OrderService>) { }

    async updateCompletionState(
        id: number,
        { isCompleted }: { isCompleted: boolean },
    ): Promise<OrderService> {
        const orderService = await this.orderServiceRepository.findOne(id);

        return await this.orderServiceRepository.save({ ...orderService, isCompleted });
    }
}
