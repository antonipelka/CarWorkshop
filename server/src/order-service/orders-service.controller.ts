import { Body, Controller, Param, Post } from '@nestjs/common';
import { OrdersServicesService } from './orders-service.service';

@Controller('orderServices')
export class OrdersServicesController {
    constructor(private readonly ordersServicesService: OrdersServicesService) { }

    @Post(':id')
    async updateCompletionState(
        @Param('id') id,
        @Body() body) {
        return this.ordersServicesService.updateCompletionState(id, body);
    }
}
