import { Module } from '@nestjs/common';
import { OrdersServicesController } from './orders-service.controller';
import { OrdersServicesService } from './orders-service.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderService } from './order-service.entity';
import { AuthModule } from 'auth/auth.module';

@Module({
    imports: [
        AuthModule,
        TypeOrmModule.forFeature([OrderService]),
    ],
    controllers: [OrdersServicesController],
    providers: [OrdersServicesService],
})
export class OrdersServicesModule { }
