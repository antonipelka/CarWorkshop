import { Body, Controller, Get, Param, Post, Req, ValidationPipe } from '@nestjs/common';
import { CreateMessageDto } from './dtos/create-message.dto';
import { MessagesService } from './messages.service';

@Controller('orders/:orderId/messages')
export class MessagesController {
    constructor(private readonly messagesService: MessagesService) { }

    @Get()
    getOrderMessages(@Param() { orderId }) {
        return this.messagesService.getMessagesByOrder(orderId);
    }

    @Post()
    async store(
        @Req() req,
        @Param() params,
        @Body(new ValidationPipe({
            validationError: {
                target: false,
                value: false,
            },
        })) createMessageDto: CreateMessageDto,
    ) {
        return this.messagesService.create({ userId: req.user.id, orderId: params.orderId, ...createMessageDto });
    }
}
