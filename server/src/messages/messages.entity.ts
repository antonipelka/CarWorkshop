import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, OneToMany, ManyToOne } from 'typeorm';
import { Order } from '../orders/order.entity';
import { User } from '../users/user.entity';

@Entity({
    name: 'messages',
})
export class Message {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    content: string;

    @CreateDateColumn()
    readonly createdAt: Date;

    @ManyToOne(type => User, user => user.messages)
    user: User;

    @ManyToOne(type => Order, order => order.messages)
    order: Order;

}