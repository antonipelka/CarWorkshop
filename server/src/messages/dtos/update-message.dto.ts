import { IsEmail, MinLength, IsString, IsBoolean, IsOptional, IsAlpha, IsNumberString, ValidateIf, IsNotEmpty } from "class-validator";
import { EqualsTo } from "shared/validation/equals-to";
import { Timestamp } from "typeorm";

export class UpdateMessageDto {
     
    @IsString()
    content: string;

}