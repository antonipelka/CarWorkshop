import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Order } from 'orders/order.entity';
import { Repository } from 'typeorm';
import { User } from 'users/user.entity';
import { CreateMessageDto } from './dtos/create-message.dto';
import { Message } from './message.entity';
import { NotificationsService } from 'notifications/notification.service';

@Injectable()
export class MessagesService {
    constructor(
        @InjectRepository(Message) private readonly messageRepository: Repository<Message>,
        @InjectRepository(Order) private readonly orderRepository: Repository<Order>,
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        private readonly notificationsService: NotificationsService,
    ) { }

    async create({
        content,
        userId,
        orderId,
    }: CreateMessageDto): Promise<Message> {
        const user = await this.userRepository.findOne(userId);
        const order = await this.orderRepository.findOne(orderId, { relations: ['client', 'employee'] });
        const message = this.messageRepository.create({
            content,
            user,
            order,
        });

        if (user.isEmployee) {
            order.employee = user;
            await this.orderRepository.save(order);
        }

        const employeeId = order.employee ? order.employee.id : null;
        const clientId = order.client ? order.client.id : null;
        const targetUserId = user.isEmployee ? clientId : employeeId;

        this.notificationsService.create({
            message: `Nowa wiadomość od ${user.firstName + ' ' + user.lastName} w zleceniu o numerze ${orderId}.`,
            orderId,
            userId: targetUserId,
        });

        return await this.messageRepository.save(message);
    }

    async getMessagesByOrder(id: number) {
        return await this.messageRepository.find({
            where: { order: id }, relations: ['user'], order: {
                createdAt: 'ASC',
            },
        });
    }
}
