import { Module } from '@nestjs/common';
import { MessagesController } from './messages.controller';
import { MessagesService } from './messages.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Message } from './message.entity';
import { AuthModule } from 'auth/auth.module';
import { Order } from 'orders/order.entity';
import { User } from 'users/user.entity';
import { NotificationsModule } from 'notifications/notification.module';

@Module({
    imports: [
        AuthModule,
        NotificationsModule,
        TypeOrmModule.forFeature([Message, Order, User]),
    ],
    controllers: [MessagesController],
    providers: [MessagesService],
})
export class MessagesModule { }
