import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, ManyToOne } from 'typeorm';
import { Order } from '../orders/order.entity';
import { User } from '../users/user.entity';

@Entity({
    name: 'messages',
})
export class Message {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    content: string;

    @CreateDateColumn()
    readonly createdAt: Date;

    @ManyToOne(() => User, user => user.messages)
    user: User;

    @ManyToOne(() => Order, order => order.messages, { onDelete: 'CASCADE' })
    order: Order;
}
