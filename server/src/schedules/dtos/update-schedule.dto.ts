import { IsEmail, MinLength, IsString, IsBoolean, IsOptional, IsAlpha, IsNumberString, ValidateIf, IsNotEmpty } from "class-validator";
import { EqualsTo } from "shared/validation/equals-to";

export class UpdateScheduleDto { // raczej nie potrzebny
    
    @IsString()
    @IsNotEmpty()
    weekDay: string;

    @IsNotEmpty()
    hour: number;
 
}