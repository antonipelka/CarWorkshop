import { IsNotEmpty, IsString } from 'class-validator';

export class CreateScheduleDto {
    @IsString()
    @IsNotEmpty()
    weekDay: string;

    @IsNotEmpty()
    hour: number;
}
