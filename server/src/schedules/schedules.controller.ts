import { Body, Controller, Delete, Get, Param, Post, ValidationPipe } from '@nestjs/common';
import { CreateScheduleDto } from './dtos/create-schedule.dto';
import { SchedulesService } from './schedules.service';

@Controller('schedules')
export class SchedulesController {
    constructor(private readonly schedulesService: SchedulesService) { }

    @Get()
    index() {
        return this.schedulesService.findAll();
    }

    @Post()
    async store(@Body(new ValidationPipe({ validationError: { target: false, value: false } })) createScheduleDto: CreateScheduleDto) {
        return this.schedulesService.create(createScheduleDto);
    }

    @Delete(':id')
    async remove(@Param() {id}){
       return this.schedulesService.remove(id);
    }
}
