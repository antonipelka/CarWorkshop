import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateScheduleDto } from './dtos/create-schedule.dto';
import { Schedule } from './schedule.entity';

@Injectable()
export class SchedulesService {
    constructor(@InjectRepository(Schedule)
    private readonly scheduleRepository: Repository<Schedule>) { }

    async findAll(): Promise<Schedule[]> {
        return await this.scheduleRepository.find();
    }

    async create({
        weekDay,
        hour,
    }: CreateScheduleDto): Promise<Schedule> {
        const order = this.scheduleRepository.create({
           weekDay,
           hour,
        });

        return await this.scheduleRepository.save(order);
    }

    async remove(id: number) {
        return await this.scheduleRepository.delete(id);
    }

    async getOrderById(id: number) {
        return await this.scheduleRepository.findOne(id);
    }
}
