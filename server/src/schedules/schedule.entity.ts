import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({
    name: 'schedules',
})
export class Schedule {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    weekDay: string; // 0-6

    @Column()
    hour: number; // 0-23
}
