import { Controller, Get, Post, Body, ValidationPipe, Delete, Patch, Put, Param, Req, UseGuards } from '@nestjs/common';
import { PartsService } from './parts.service';
import { CreatePartDto } from './dtos/create-part.dto';
import { UpdatePartDto } from './dtos/update-part.dto';
import { EmployeeGuard } from '../shared/authorization/employee.guard';

@Controller('parts')
export class PartsController {
    constructor(private readonly ordersService: PartsService) { }

    @Get()
    @UseGuards(EmployeeGuard)
    index() {
        return this.ordersService.findAll();
    }

    @Post()
    @UseGuards(EmployeeGuard)
    async store(@Body(new ValidationPipe({ validationError: { target: false, value: false } })) createOrderDto: CreatePartDto) {
        return this.ordersService.create(createOrderDto);
    }

    @Delete(':id')
    @UseGuards(EmployeeGuard)
    async remove(@Param() { id }) {
        return this.ordersService.remove(id);
    }

    @Put(':id')
    @UseGuards(EmployeeGuard)
    async update(@Param() params, @Body() updateOrderDto: UpdatePartDto) {
        return this.ordersService.update(params.id, updateOrderDto);
    }
}
