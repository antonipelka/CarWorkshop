import { IsString } from 'class-validator';

export class CreatePartDto {
    @IsString()
    name: string;
    count: number;
}
