import { IsString } from 'class-validator';

export class UpdatePartDto {
    @IsString()
    name: string;
    count: number;
    serviceIds: number[];
}
