import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Service } from '../services/service.entity';

@Entity({
    name: 'parts',
})
export class Part {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    count: number;

    @ManyToMany(() => Service, services => services.parts)
    services: Service[];
}
