import { Module } from '@nestjs/common';
import { PartsController } from './parts.controller';
import { PartsService } from './parts.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Part } from './part.entity';
import { AuthModule } from 'auth/auth.module';

@Module({
    imports: [
        AuthModule,
        TypeOrmModule.forFeature([Part]),
    ],
    controllers: [PartsController],
    providers: [PartsService],
})
export class PartsModule { }