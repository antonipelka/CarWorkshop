import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreatePartDto } from './dtos/create-part.dto';
import { UpdatePartDto } from './dtos/update-part.dto';
import { Part } from './part.entity';

@Injectable()
export class PartsService {
    constructor(@InjectRepository(Part)
    private readonly partRepository: Repository<Part>) { }

    async findAll(): Promise<Part[]> {
        return await this.partRepository.find();
    }

    async create({
        name,
        count,
    }: CreatePartDto): Promise<Part> {
        const part = this.partRepository.create({
            name,
            count,
        });

        return await this.partRepository.save(part);
    }

    async remove(id: number) {
        return await this.partRepository.delete(id);
    }

    async update(
        id: number,
        updatePartDto: UpdatePartDto,
    ): Promise<Part> {
        const part = await this.getPartById(id);

        return await this.partRepository.save({ ...part, ...updatePartDto });
    }

    async getPartById(id: number) {
        return await this.partRepository.findOne(id);
    }
}
