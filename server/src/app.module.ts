import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'auth/auth.module';
import { CarsModule } from 'cars/cars.module';
import { MailModule } from 'mail/mail.module';
import { MessagesModule } from 'messages/messages.module';
import { NotificationsModule } from 'notifications/notification.module';
import { OrdersServicesModule } from 'order-service/orders-service.module';
import { OrdersModule } from 'orders/orders.module';
import { PartsModule } from 'parts/parts.module';
import { SchedulesModule } from 'schedules/schedules.module';
import { ServicesModule } from 'services/services.module';
import { UsersModule } from 'users/users.module';
import { AppController } from './app.controller';

@Module({
    imports: [
        TypeOrmModule.forRoot(),
        AuthModule,
        UsersModule,
        OrdersModule,
        OrdersServicesModule,
        CarsModule,
        MessagesModule,
        NotificationsModule,
        ServicesModule,
        PartsModule,
        SchedulesModule,
        MailModule,
    ],
    controllers: [AppController],
    providers: [],
})
export class AppModule { }
