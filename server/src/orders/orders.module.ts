import { Module } from '@nestjs/common';
import { OrdersController } from './orders.controller';
import { OrdersService } from './orders.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './order.entity';
import { AuthModule } from '../auth/auth.module';
import { Service } from '../services/service.entity';
import { Car } from '../cars/car.entity';
import { User } from '../users/user.entity';
import { Message } from '../messages/message.entity';
import { Notification } from '../notifications/notification.entity';
import { Schedule } from '../schedules/schedule.entity';
import { OrderStatesController } from './order-states.controller';
import { NotificationsModule } from 'notifications/notification.module';

@Module({
    imports: [
        AuthModule,
        NotificationsModule,
        TypeOrmModule.forFeature([Order, Service, Car, User, Message, Notification, Schedule]),
    ],
    controllers: [OrdersController, OrderStatesController],
    providers: [OrdersService],
    exports: [OrdersService],
})
export class OrdersModule { }
