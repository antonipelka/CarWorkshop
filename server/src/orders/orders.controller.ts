import { Body, Controller, Delete, Get, Param, Post, Put, Req, ValidationPipe } from '@nestjs/common';
import { Response } from 'express';
import { CreateOrderDto } from './dtos/create-order.dto';
import { UpdateOrderDto } from './dtos/update-order.dto';
import { OrdersService } from './orders.service';

@Controller('orders')
export class OrdersController {
    constructor(
        private readonly ordersService: OrdersService,
    ) { }

    @Get('terms')
    async getReservedTerms() {
        return this.ordersService.getReservedTerms();
    }

    @Get()
    async index(@Req() req) {
        const orders = await this.ordersService.findAll(req.user.id);
        const res = req.res as Response;
        res.set('X-Total-Count', orders.filter(order => order.state !== 'finished').length as any);
        return orders;
    }

    @Post()
    async store(@Req() req, @Body(new ValidationPipe({ validationError: { target: false, value: false } })) createOrderDto: CreateOrderDto) {
        return this.ordersService.create({ clientId: req.user.id, ...createOrderDto });
    }

    @Delete(':id')
    async remove(@Param() { id }) {
        return this.ordersService.remove(id);
    }

    @Put(':id')
    async update(@Param() { id }, @Body() updateOrderDto: UpdateOrderDto, @Req() req) {
        return this.ordersService.update(id, updateOrderDto, req.user.id);
    }

    @Get(':id')
    async getOrder(@Param('id') id, @Req() req) {
        return this.ordersService.getOrderById(id, req.user.id);
    }

    @Get('client/:id')
    async getClientOrders(@Param('id') id) {
        return this.ordersService.getOrdersByClient(id);
    }

}
