import { Controller, Get, NotFoundException, Param } from '@nestjs/common';
import { OrdersService } from './orders.service';

@Controller('publicOrders')
export class OrderStatesController {
    constructor(
        private readonly ordersService: OrdersService,
    ) { }

    @Get(':id')
    async getOrderState(@Param('id') id) {
        const order = await this.ordersService.getOrderState(+id);
        if (!order) {
            throw new NotFoundException();
        }

        return order;
    }
}
