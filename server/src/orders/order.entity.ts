import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, CreateDateColumn } from 'typeorm';
import { Car } from '../cars/car.entity';
import { Message } from '../messages/message.entity';
import { Notification } from '../notifications/notification.entity';
import { OrderService } from '../order-service/order-service.entity';
import { User } from '../users/user.entity';

@Entity({
    name: 'orders',
})
export class Order {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        unique: true,
    })
    term: Date;

    @CreateDateColumn()
    createdAt: Date;

    @Column()
    comment: string;

    @Column()
    state: string;

    @ManyToOne(type => Car, { onDelete: 'CASCADE' })
    car: Car;

    @ManyToOne(type => User, user => user.orders)
    client: User;

    @ManyToOne(type => User, user => user.orders)
    employee: User;

    @OneToMany(
        type => OrderService,
        (orderServices: OrderService) => orderServices.order,
        { cascade: true },
    )
    orderServices: OrderService[];

    @OneToMany(type => Message, message => message.order)
    messages: Message[];

    @OneToMany(type => Notification, notification => notification.order)
    notifications: Notification[];

}
