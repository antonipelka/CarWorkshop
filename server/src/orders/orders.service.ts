import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In } from 'typeorm';
import { Car } from '../cars/car.entity';
import { OrderService } from '../order-service/order-service.entity';
import { Schedule } from '../schedules/schedule.entity';
import { Service } from '../services/service.entity';
import { User } from '../users/user.entity';
import { CreateOrderDto } from './dtos/create-order.dto';
import { ReservedTermsDto } from './dtos/reserved-terms.dto';
import { UpdateOrderDto } from './dtos/update-order.dto';
import { Order } from './order.entity';
import { } from 'typeorm';
import { NotificationsService } from 'notifications/notification.service';

@Injectable()
export class OrdersService {
    constructor(
        @InjectRepository(Order) private readonly orderRepository: Repository<Order>,
        @InjectRepository(Service) private readonly serviceRepository: Repository<Service>,
        @InjectRepository(Car) private readonly carRepository: Repository<Car>,
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        @InjectRepository(Schedule) private readonly scheduleRepository: Repository<Schedule>,
        private readonly notificationsService: NotificationsService,
    ) { }

    async findAll(userId: number): Promise<Order[]> {
        const user = await this.userRepository.findOne(userId);
        return await this.orderRepository.find({ where: user.isEmployee ? {} : { client: user.id }, relations: ['client'] });
    }

    async create({
        term,
        comment,
        carId,
        clientId,
        serviceIds,
    }: CreateOrderDto): Promise<Order> {
        const order = this.orderRepository.create({
            term,
            comment,
            car: await this.carRepository.findOne(carId),
            state: 'created',
            client: await this.userRepository.findOne(clientId),
            orderServices: await this.mapServiceIdsToOrderServices(serviceIds),
        });

        return await this.orderRepository.save(order);
    }

    async remove(id: number) {
        return await this.orderRepository.delete(id);
    }

    async update(
        id: number,
        {
            carId,
            comment,
            serviceIds,
            state,
            term,
        }: UpdateOrderDto,
        userId: number,
    ): Promise<Order> {
        const order = await this.orderRepository.findOne(id, { relations: ['client', 'orderServices', 'orderServices.service'] });
        let orderServices = await this.mapServiceIdsToOrderServices(serviceIds);
        orderServices = orderServices.map(orderService => {
            const existingOrderService = order.orderServices.find(persistedOrderService => {
                return persistedOrderService.service.id === orderService.service.id;
            });
            return existingOrderService || orderService;
        });

        await this.orderRepository.save({
            ...order, ...{
                car: { id: carId },
                comment,
                employee: { id: userId },
                term,
                orderServices,
                state,
            },
        });

        this.notificationsService.create({
            message: `Twoje zlecenie o numerze ${id} zostało zmodyfikowane.`,
            orderId: id,
            userId: order.client.id,
        });

        return await this.getOrderById(id);
    }

    private async mapServiceIdsToOrderServices(serviceIds: number[]) {
        const services = await this.serviceRepository.findByIds(serviceIds);
        return services.map(service => {
            const orderService = new OrderService();
            orderService.isCompleted = false;
            orderService.service = service;
            return orderService;
        });
    }

    async getOrderById(id: number, userId: number = null) {
        if (userId) {
            this.notificationsService.markOrderNotificationsAsRead(id, userId);
        }

        return await this.orderRepository.findOne(id, {
            relations: ['client', 'car'],
            join: {
                alias: 'order',
                leftJoinAndSelect: {
                    orderServices: 'order.orderServices',
                    services: 'orderServices.service',
                },
            },
        });
    }

    async getOrdersByClient(id: number) {
        return await this.orderRepository.find({ where: { client: id }, relations: ['car'] });
    }

    async getReservedTerms() {
        const terms = await this.orderRepository.find({ select: ['term'] });
        const schedules = await this.scheduleRepository.find();
        return new ReservedTermsDto({ terms, schedules });
    }

    async getOrderState(id: number) {
        return Number.isInteger(id) ? await this.orderRepository.findOne(id, {
            select: ['id', 'state'],
            join: {
                alias: 'order',
                leftJoinAndSelect: {
                    orderServices: 'order.orderServices',
                    services: 'orderServices.service',
                },
            },
        }) : null;
    }

    async getLastOrdersByClientIds(clientIds: number[]) {
        const queryBuilder = this.orderRepository.createQueryBuilder('orders')
            .where('orders."clientId" IN (:...clientIds)', { clientIds })
            .where(qb => {
                const subQuery = qb.subQuery()
                    .select('last_orders.id')
                    .from(Order, 'last_orders')
                    .where('last_orders."clientId" = orders."clientId"')
                    .orderBy('last_orders."createdAt"', 'DESC')
                    .limit(1);
                return 'orders.id IN (' + subQuery.getQuery() + ')';
            })
            .leftJoinAndSelect('orders.client', 'client')
            .leftJoinAndSelect('orders.car', 'car')
            .orderBy('orders."createdAt"', 'DESC');

        return await queryBuilder.getMany();
    }

}
