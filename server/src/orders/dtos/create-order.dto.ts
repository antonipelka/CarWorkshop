import { IsISO8601, IsNotEmpty, IsString } from 'class-validator';

export class CreateOrderDto {
    @IsISO8601()
    term: Date;

    @IsString()
    comment: string;

    @IsNotEmpty()
    carId: number;

    clientId?: number;

    serviceIds: number [] = [];

}
