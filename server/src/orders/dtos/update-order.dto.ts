import { IsNotEmpty, IsString } from 'class-validator';
import { Timestamp } from 'typeorm';

export class UpdateOrderDto {

    @IsString()
    readonly term: Date;

    @IsString()
    readonly comment: string;

    @IsString()
    state: string;

    @IsNotEmpty()
    carId?: number;

    employeeId?: number;
    serviceIds?: number[];
}
