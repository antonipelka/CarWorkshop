import { Order } from '../order.entity';
import { Schedule } from '../../schedules/schedule.entity';

export class ReservedTermsDto {
    terms: Partial<Order>[];
    schedules: Schedule[];

    constructor(object?: {
        terms: Partial<Order>[],
        schedules: Schedule[],
    }) {
        Object.assign(this, object);
    }
}