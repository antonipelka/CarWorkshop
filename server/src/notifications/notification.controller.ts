import { Controller, Get, Req } from '@nestjs/common';
import { Response } from 'express';
import { NotificationsService } from './notification.service';

@Controller('notifications')
export class NotificationsController {
    constructor(private readonly notificationsService: NotificationsService) { }

    @Get()
    async index(@Req() req) {
        const notifications = await this.notificationsService.findAll(req.user.id);
        const res = req.res as Response;
        res.set('X-Total-Count', notifications.filter(notification => notification.readAt === null).length as any);
        return notifications;
    }
}
