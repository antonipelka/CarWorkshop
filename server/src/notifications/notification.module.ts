import { Module, HttpService } from '@nestjs/common';
import { NotificationsController } from './notification.controller';
import { NotificationsService } from './notification.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'auth/auth.module';
import { Order } from 'orders/order.entity';
import { Notification } from 'notifications/notification.entity';
import { User } from 'users/user.entity';
import { MailService } from 'mail/mail.service';
import { MailModule } from 'mail/mail.module';

@Module({
    imports: [
        AuthModule,
        MailModule,
        TypeOrmModule.forFeature([Notification, Order, User]),
    ],
    controllers: [NotificationsController],
    providers: [NotificationsService],
    exports: [NotificationsService],
})
export class NotificationsModule { }
