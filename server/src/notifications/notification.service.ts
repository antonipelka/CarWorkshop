import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateNotificationDto } from './dtos/create-notification.dto';
import { Notification } from './notification.entity';
import { MailService } from 'mail/mail.service';

@Injectable()
export class NotificationsService {
    constructor(
        @InjectRepository(Notification) private readonly notificationRepository: Repository<Notification>,
        private readonly mailService: MailService,
    ) { }

    async findAll(userId: number): Promise<Notification[]> {
        return await this.notificationRepository.find({
            where: {
                user: userId,
            },
            relations: ['order'],
            order: {
                createdAt: 'DESC',
            },
        });
    }

    async create({
        message,
        userId,
        orderId,
    }: CreateNotificationDto): Promise<Notification> {
        let notification;
        if (userId) {
            notification = this.notificationRepository.create({
                message,
                user: { id: userId },
                order: { id: orderId },
            });

            notification = await this.notificationRepository.save(notification);
            this.mailService.sendNotificationMessage(notification);
        }
        return notification;
    }

    async markOrderNotificationsAsRead(orderId: number, userId: number) {
        await this.notificationRepository.update(
            {
                order: { id: orderId },
                user: { id: userId },
            }, {
                readAt: new Date(),
            },
        );
    }
}
