export class CreateNotificationDto {
    message: string;
    userId: number;
    orderId: number;
}
