import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, ManyToOne } from 'typeorm';
import { Order } from '../orders/order.entity';
import { User } from '../users/user.entity';

@Entity({
    name: 'notifications',
})
export class Notification {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    message: string;

    @CreateDateColumn()
    createdAt: Date;

    @Column({
        nullable: true,
    })
    readAt: Date;

    @ManyToOne(() => User, user => user.notifications)
    user: User;

    @ManyToOne(() => Order, order => order.notifications, { onDelete: 'CASCADE' })
    order: Order;
}
