import { Body, Controller, Get, Post, Req, UseGuards, ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { GenerateTokenDto } from './dtos/generate-token.dto';
import { ResetPasswordDto } from './dtos/reset-password.dto';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) { }

    @Post('token')
    async store(@Body() generateTokenDto: GenerateTokenDto) {
        return this.authService.createToken(generateTokenDto);
    }

    @Get('parseToken')
    @UseGuards(AuthGuard('jwt'))
    async parseToken(@Req() request) {
        return this.authService.getAuthenticatedUserById(request.user.id);
    }

    @Post('resetPassword')
    async resetPassword(@Body('email') email) {
        return this.authService.resetPassword(email);
    }

    @Post('updatePassword')
    async updatePassword(@Body(new ValidationPipe({
        validationError: { target: false, value: false },
    })) resetPasswordDto: ResetPasswordDto) {
        return this.authService.updatePassword(resetPasswordDto);
    }
}
