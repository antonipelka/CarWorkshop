import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CarsController } from 'cars/cars.controller';
import { MailModule } from 'mail/mail.module';
import { OrdersController } from 'orders/orders.controller';
import { PartsController } from 'parts/parts.controller';
import * as passport from 'passport';
import { SchedulesController } from 'schedules/schedules.controller';
import { ServicesController } from 'services/services.controller';
import { User } from '../users/user.entity';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtStrategy } from './strategies/jwt.strategy';
import { NotificationsController } from 'notifications/notification.controller';
import { OrdersServicesController } from 'order-service/orders-service.controller';
import { MessagesController } from 'messages/messages.controller';

@Module({
    imports: [TypeOrmModule.forFeature([User]), MailModule],
    controllers: [AuthController],
    providers: [AuthService, JwtStrategy],
    exports: [AuthService],
})
export class AuthModule implements NestModule {
    public configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(passport.authenticate('jwt', { session: false }))
            .forRoutes(
                OrdersController,
                CarsController,
                ServicesController,
                PartsController,
                SchedulesController,
                NotificationsController,
                OrdersServicesController,
                MessagesController,
            );
    }
}
