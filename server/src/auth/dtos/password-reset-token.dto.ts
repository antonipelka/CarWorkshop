import { IsString, MinLength, IsNumber, IsEmail } from "class-validator/decorator/decorators";

export class PasswordResetDto {

@IsEmail()
 email: string;

@IsNumber()
code: number;

@IsString()
@MinLength(6)
password: string = '';

}