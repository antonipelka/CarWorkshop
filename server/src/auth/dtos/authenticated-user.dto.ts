export class AuthenticatedUserDto {
    id: number;
    firstName: string;
    lastName: string;
    isCompany: boolean;
    companyName: string;
    isEmployee: boolean;

    constructor(object: AuthenticatedUserDto) {
        Object.assign(this, object);
    }
}