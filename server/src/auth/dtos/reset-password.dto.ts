import { EqualsTo } from '../../shared/validation/equals-to';
import { MinLength, IsString } from 'class-validator';

export class ResetPasswordDto {
    code: string;
    @IsString()
    @MinLength(6)
    password: string;
    @IsString()
    @EqualsTo('password', {
        message: 'passwordConfirmation must be equal to password',
    })
    passwordConfirmation: string;
}
