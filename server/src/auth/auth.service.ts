import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcryptjs';
import { ValidationError } from 'class-validator';
import * as jwt from 'jsonwebtoken';
import { Repository } from 'typeorm';
import { User } from '../users/user.entity';
import { GenerateTokenDto } from './dtos/generate-token.dto';
import { GeneratedTokenDto } from './dtos/generated-token.dto';
import { AuthenticatedUserDto } from './dtos/authenticated-user.dto';
import { MailService } from '../mail/mail.service';
import { ResetPasswordDto } from './dtos/reset-password.dto';

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        private readonly mailService: MailService,
    ) { }

    async createToken({ email, password }: GenerateTokenDto): Promise<GeneratedTokenDto> {
        const expiresIn = 60 * 60, secretOrKey = 'secret';

        const user = await this.userRepository.createQueryBuilder('user')
            .where('user.email = :email', { email })
            .addSelect('user.password')
            .getOne();

        if (!user || !(await bcrypt.compare(password, user.password))) {
            const error = new ValidationError();
            error.property = 'general';
            error.children = [];
            error.constraints = {
                invalid: 'invalid credentials',
            };
            throw new BadRequestException([error]);
        }

        const token = jwt.sign({ id: user.id }, secretOrKey, { expiresIn });
        return {
            expiresIn,
            token,
            user: this.mapUserToAuthenticatedUser(user),
        };
    }

    async validateUser(signedUser): Promise<boolean> {
        return !!(await this.userRepository.findOne({
            id: signedUser.id,
        }));
    }

    async getAuthenticatedUserById(id: number) {
        return this.mapUserToAuthenticatedUser(await this.userRepository.findOne(id));
    }

    private mapUserToAuthenticatedUser(user: User) {
        return new AuthenticatedUserDto({
            id: user.id,
            companyName: user.companyName,
            firstName: user.firstName,
            isCompany: user.isCompany,
            lastName: user.lastName,
            isEmployee: user.isEmployee,
        });
    }

    async generateRandomCode(key) {
        let code: string, match: User;
        do {
            code = await Math.random().toString(36).substring(6);
            match = await this.userRepository.findOne({ where: { [key]: code } });
        } while (match);

        return code;
    }

    async resetPassword(email: string) {
        const resetToken = await this.generateRandomCode('resetToken');
        const user = await this.userRepository.findOne({ where: { email } });
        if (user) {
            user.resetToken = resetToken;
            await this.userRepository.save(user);
            this.mailService.sendPasswordResetLink(user.email, resetToken);
        }
    }

    async updatePassword(resetPasswordDto: ResetPasswordDto) {
        const user = await this.userRepository.findOne({ where: { resetToken: resetPasswordDto.code } });
        if (user) {
            user.resetToken = null;
            user.password = await bcrypt.hash(resetPasswordDto.password, 10);
        }
    }
}
