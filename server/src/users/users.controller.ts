import { Body, Controller, Get, Param, Post, ValidationPipe, Put, Query, Req, UseGuards } from '@nestjs/common';
import { CreateUserDto } from './dtos/create-user.dto';
import { UsersService } from './users.service';
import { UpdateUserDto } from './dtos/update-user.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('users')
export class UsersController {
    constructor(private readonly usersService: UsersService) { }

    @Get()
    @UseGuards(AuthGuard('jwt'))
    index(@Query('type') type) {
        return type === 'client'
            ? this.usersService.getClients()
            : this.usersService.findAll();
    }

    @Post()
    async store(@Body(new ValidationPipe({
        validationError: { target: false, value: false },
    })) createUserDto: CreateUserDto) {
        return this.usersService.create(createUserDto);
    }

    @Post('activate/:code')
    async activate(@Param('code') code) {
        return this.usersService.activateUser(code);
    }

    @Get('profile')
    @UseGuards(AuthGuard('jwt'))
    async getUserProfile(@Req() req) {
        return this.usersService.getUserDetails(req.user.id);
    }

    @Get(':id')
    @UseGuards(AuthGuard('jwt'))
    async getClientDetails(@Param('id') id) {
        return this.usersService.getUserDetails(id);
    }

    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    async update(@Param() params, @Body() updateOrderDto: UpdateUserDto, @Req() req) {
        return this.usersService.update(params.id, updateOrderDto, req.user.id);
    }
}
