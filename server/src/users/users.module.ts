import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'auth/auth.module';
import { OrdersModule } from 'orders/orders.module';
import { Car } from '../cars/car.entity';
import { Message } from '../messages/message.entity';
import { Notification } from '../notifications/notification.entity';
import { Order } from '../orders/order.entity';
import { User } from './user.entity';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { MailModule } from 'mail/mail.module';

@Module({
    imports: [TypeOrmModule.forFeature([User, Order, Message, Notification, Car]), OrdersModule, AuthModule, MailModule],
    controllers: [UsersController],
    providers: [UsersService],
})
export class UsersModule { }
