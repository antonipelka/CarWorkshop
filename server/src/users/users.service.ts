import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcryptjs';
import { ValidationError } from 'class-validator';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dtos/create-user.dto';
import { User } from './user.entity';
import { UpdateUserDto } from './dtos/update-user.dto';
import { AuthService } from '../auth/auth.service';
import { OrdersService } from 'orders/orders.service';
import { MailService } from '../mail/mail.service';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        private readonly ordersService: OrdersService,
        private readonly authService: AuthService,
        private readonly mailService: MailService,
    ) { }

    async findAll(): Promise<User[]> {
        return await this.userRepository.find();
    }

    async create({
        email,
        password,
        firstName,
        lastName,
        identificationNumber,
        phoneNumber,
        address,
        isCompany,
        companyName,
    }: CreateUserDto): Promise<User> {
        if (await this.userRepository.findOne({ email })) {
            const error = new ValidationError();
            error.property = 'email';
            error.children = [];
            error.constraints = {
                unique: 'email must be unique',
            };
            throw new BadRequestException([error]);
        }

        const userCount = await this.userRepository.count();

        const activationToken = await this.authService.generateRandomCode('activationToken');
        const user = this.userRepository.create({
            email,
            password: await bcrypt.hash(password, 10),
            firstName,
            lastName,
            identificationNumber,
            phoneNumber,
            address,
            isCompany,
            companyName,
            activationToken,
            isEmployee: !userCount,
        });
        this.mailService.sendActivationLink(email, activationToken);

        return await this.userRepository.save(user);
    }

    async getClients() {
        const clients = await this.userRepository.find({ where: { isEmployee: false } });
        const lastOrders = await this.ordersService.getLastOrdersByClientIds(clients.map(client => client.id));
        clients.forEach(client => {
            client.lastOrder = lastOrders.find(lastOrder => lastOrder.client.id === client.id);
        });

        return clients;
    }

    async getUserDetails(id: number) {
        return await this.userRepository.findOne(id);
    }

    async update(
        id: number,
        object: UpdateUserDto,
        userId: number,
    ): Promise<User> {
        const user = await this.userRepository.findOne(userId);
        if (!user.isEmployee) {
            delete object.isEmployee;
        }
        await this.userRepository.update(id, object);

        return await this.getUserById(id);
    }

    async getUserById(id: number) {
        return await this.userRepository.findOne(id);
    }

    async activateUser(code: string) {
        return await this.userRepository.update({ activationToken: code }, { activationToken: null });
    }
}
