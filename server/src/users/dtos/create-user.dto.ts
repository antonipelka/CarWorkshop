import { Equals, IsAlpha, IsBoolean, IsEmail, IsNotEmpty, IsOptional, IsString, MinLength, ValidateIf } from 'class-validator';
import { EqualsTo } from '../../shared/validation/equals-to';

export class CreateUserDto {
    @IsEmail()
    readonly email: string = '';

    @IsString()
    @MinLength(6)
    readonly password: string = '';

    @IsString()
    @EqualsTo('password', {
        message: 'passwordConfirmation must be equal to password',
    })
    readonly passwordConfirmation: string = '';

    @IsString()
    @IsAlpha()
    @MinLength(2)
    readonly firstName: string = '';

    @IsString()
    @IsAlpha()
    @MinLength(2)
    readonly lastName: string = '';

    @IsOptional()
    @IsString()
    readonly phoneNumber: string = '';

    @IsBoolean()
    readonly isCompany: boolean = false;

    @ValidateIf(o => o.isCompany)
    @IsString()
    readonly companyName: string = '';

    @IsString()
    @ValidateIf(o => o.isCompany)
    @IsNotEmpty()
    readonly identificationNumber: string = '';

    @IsOptional()
    @IsString()
    readonly address: string = '';

    @IsBoolean()
    @Equals(true)
    readonly rulesAgreement: boolean = false;
}
