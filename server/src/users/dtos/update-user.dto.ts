import { IsAlpha, IsBoolean, IsEmail, IsNotEmpty, IsOptional, IsString, MinLength, ValidateIf } from 'class-validator';

export class UpdateUserDto {
    @IsEmail()
    email?: string = '';

    @IsString()
    @IsAlpha()
    @MinLength(2)
    firstName?: string = '';

    @IsString()
    @IsAlpha()
    @MinLength(2)
    lastName?: string = '';

    @IsOptional()
    @IsString()
    phoneNumber?: string = '';

    @IsBoolean()
    isCompany?: boolean = false;

    @ValidateIf(o => o.isCompany)
    @IsString()
    companyName?: string = '';

    @IsString()
    @ValidateIf(o => o.isCompany)
    @IsNotEmpty()
    identificationNumber?: string = '';

    @IsOptional()
    @IsString()
    address?: string = '';

    @IsOptional()
    @IsBoolean()
    isEmployee?: boolean = false;
}
