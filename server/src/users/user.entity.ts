import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, OneToMany } from 'typeorm';
import { Order } from '../orders/order.entity';
import { Car } from '../cars/car.entity';
import { Message } from '../messages/message.entity';
import { Notification } from '../notifications/notification.entity';

@Entity({
    name: 'users',
})
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        unique: true,
    })
    email: string;

    @Column({
        select: false,
    })
    password: string;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column({
        nullable: true,
    })
    phoneNumber: string;

    @Column({
        nullable: true,
    })
    address: string;

    @Column({
        default: false,
    })
    isCompany: boolean;

    @Column({
        nullable: true,
    })
    companyName: string;

    @Column({
        nullable: true,
    })
    identificationNumber: string;

    @CreateDateColumn()
    createdAt: Date;

    @Column({
        nullable: true,
        select: false,
    })
    activationToken: string;

    @Column({
        nullable: true,
        select: false,
    })
    resetToken: string;

    @Column({
        default: false,
    })
    isEmployee: boolean;

    @OneToMany(type => Order,  order => order.client)
    orders: Order[];

    @OneToMany(type => Car,  car => car.user)
    cars: Car[];

    @OneToMany(type => Message,  messages => messages.user)
    messages: Message[];

    @OneToMany(type => Notification,  notifications => notifications.user)
    notifications: Notification[];

    lastOrder?: Order;
}
