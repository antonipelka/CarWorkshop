import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MailService } from '../mail/mail.service';
import { Notification } from '../notifications/notification.entity';
import { User } from '../users/user.entity';

@Module({
    imports: [
        // AuthModule,
        TypeOrmModule.forFeature([Notification, User]),
    ],
    exports: [
        MailService,
    ],
    controllers: [],
    providers: [MailService],
})
export class MailModule { }
