import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { User } from '../users/user.entity';
import { Notification } from '../notifications/notification.entity';
import { InjectRepository } from '@nestjs/typeorm';
import * as sgMail from '@sendgrid/mail';
// tslint:disable-next-line:no-var-requires
const config = require('../../config.json');

@Injectable()
export class MailService {
    private clientUrl = process.env.CLIENT_URL ? process.env.CLIENT_URL : config.CLIENT_URL;

    constructor(
        @InjectRepository(User) private userRepository: Repository<User>,
        @InjectRepository(Notification) private notificationRepository: Repository<Notification>,
    ) {
        sgMail.setApiKey(process.env.SG_KEY || config.SG_KEY);
    }

    private async getUserEmail(id: number) {
        const { email } = await this.userRepository.findOne(id, { select: ['email'] });
        return email;
    }

    createClientUrl(urlPart: string) {
        return this.clientUrl + urlPart;
    }

    async sendActivationLink(email: string, code: string) {
        const activationLink = this.createClientUrl('/activate/' + code);
        const msg = {
            to: email,
            from: 'no-reply@serwis-samochodowy.dev',
            subject: 'Serwis Samochodowy - Link aktywacyjny',
            text: 'Witaj,\nTwój link aktywacyjny:\n' + activationLink,
        };
        await sgMail.send(msg);
    }

    async sendPasswordResetLink(email: string, code: string) {
        const passwordResetLink = this.createClientUrl('/reset/' + code);
        const msg = {
            to: email,
            from: 'no-reply@serwis-samochodowy.dev',
            subject: 'Serwis Samochodowy - Link do resetu hasła',
            text: 'Witaj,\nTwój link do resetu hasła:\n' + passwordResetLink,
        };
        await sgMail.send(msg);
    }

    async sendNotificationMessage(notification: Notification) {
        const email = await this.getUserEmail(notification.user.id);

        const msg = {
            to: email,
            from: 'no-reply@serwis-samochodowy.dev',
            subject: 'Serwis Samochodowy - Powiadomienie dotyczące twojego zlecenia',
            text: notification.message,
        };
        await sgMail.send(msg);
    }
}
