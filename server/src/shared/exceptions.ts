import { BadRequestException } from '@nestjs/common';
import { ValidationError } from 'class-validator';
export { ValidationError } from 'class-validator';

export class InvalidRequestException extends BadRequestException {
    message: ValidationError[];
}
