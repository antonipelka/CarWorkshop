import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AuthService } from '../../auth/auth.service';

@Injectable()
export class EmployeeGuard implements CanActivate {
  constructor(private authService : AuthService) {}

  async canActivate(context: ExecutionContext) {


    const request = context.switchToHttp().getRequest();
    const user = request.user;

    const authenticatedUser = await this.authService.getAuthenticatedUserById(user.id);

    return authenticatedUser.isEmployee;
  }
}