import { Component, TemplateRef, OnInit } from '@angular/core';
import { GenerateTokenDto } from 'app/dtos/auth/generate-token.dto';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { RegistrationComponent } from '../registration/registration.component';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    credentialsModel = new GenerateTokenDto;
    errors: any[] = [];

    loginModal: BsModalRef;
    resetPasswordModal: BsModalRef;
    registrationComponent: RegistrationComponent;

    resetEmail = '';

    constructor(private modalService: BsModalService, private authenticationService: AuthenticationService, private router: Router) { }

    ngOnInit() { }

    openLoginModal(template: TemplateRef<any>) {
        this.loginModal = this.modalService.show(template, { class: 'modal-sm' });
    }

    openRegistrationModal(template: TemplateRef<any>) {
        this.registrationComponent.registrationModal = this.modalService.show(template, { class: 'modal-sm' });
    }

    closeLoginModal() {
        this.credentialsModel = new GenerateTokenDto;
        this.errors = [];
        this.loginModal.hide();
        this.loginModal = null;
    }

    async onSubmit() {
        this.errors = [];
        const signInResult = await this.authenticationService.signIn(this.credentialsModel);
        if (signInResult.success) {
            this.closeLoginModal();
            this.router.navigate(['dashboard']);
        } else {
            this.errors = signInResult.errors;
        }
    }

    openResetPasswordModal(template: TemplateRef<any>) {
        this.closeLoginModal();
        this.resetPasswordModal = this.modalService.show(template, { class: 'second' });
    }

    async submitPasswordReset() {
        this.authenticationService.resetPassword(this.resetEmail).subscribe(() => {
            this.resetPasswordModal.hide();
        });
    }

    // DEBUG
    signInAsWorker() {
        this.credentialsModel.email = 'w@w.com';
        this.credentialsModel.password = 'wwwwww';
    }
    signInAsClient() {
        this.credentialsModel.email = 'c@c.com';
        this.credentialsModel.password = 'cccccc';
    }

}
