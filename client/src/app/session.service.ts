import { Injectable } from '@angular/core';

export enum UserType {
    guest = 0,
    worker = 1,
    client = 2
  }

@Injectable()
export class SessionService {

    private username: string = 'guest';
    private token: string = 'none';
    private userType: UserType = UserType.guest;

    constructor() {}

    signIn(username: string, password: string) {
        //TODO: zaimplementować

        if (username == "w@w.w"    /* && password == 'w'*/) {
            this.username = username;
            this.userType = UserType.worker;           
        }
        else if (username == 'c@c.c' /*&& password == 'c'*/) {
            this.username = username;
            this.userType = UserType.client;
        }
    }

    signOut() {
        this.username = 'guest';
        this.token = 'none';
        this.userType = UserType.guest;
    }

    getUsername(): string {
        return this.username;
    }

    getToken(): string {
        return this.token;
    }

    getUserType(): UserType {
        return this.userType;
    }

}