export class Schedule {
    id: number;
    weekDay: string;
    hour: number;
}
