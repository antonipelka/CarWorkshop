import { Order } from '../orders/order.entity';
import { Car } from '../cars/car.entity';
import { Message } from '../messages/message.entity';
import { Notification } from '../notifications/notification.entity';

export class User {
    id: number;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    address: string;
    isCompany: boolean;
    companyName: string;
    identificationNumber: string;
    createdAt: Date;
    activationToken: string;
    resetToken: string;
    isEmployee: boolean;
    orders: Order[];
    cars: Car[];
    messages: Message[];
    notifications: Notification[];
    lastOrder?: Order;
}
