import { OrderService } from '../order-service/order-service.entity';
import { Part } from '../parts/part.entity';

export class Service {
    id: number;
    name: string;
    price: number;
    description: string;
    isChoosable: boolean;
    orderServices: OrderService[];
    parts: Part[];
}
