import { User } from '../users/user.entity';

export class Car {
    id: number;
    name: string;
    user: User;
}
