import { Service } from '../services/service.entity';

export class Part {
    id: number;
    name: string;
    count: number;
    services: Service[];
}
