import { Order } from '../orders/order.entity';
import { Service } from '../services/service.entity';

export class OrderService {
    id: number;
    isCompleted: boolean;
    order: Order;
    service: Service;
}
