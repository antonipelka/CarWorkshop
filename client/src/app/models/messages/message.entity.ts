import { Order } from '../orders/order.entity';
import { User } from '../users/user.entity';

export class Message {
    id: number;
    content: string;
    readonly createdAt: Date;
    user: User;
    order: Order;
}
