import { Car } from '../cars/car.entity';
import { Message } from '../messages/message.entity';
import { Notification } from '../notifications/notification.entity';
import { OrderService } from '../order-service/order-service.entity';
import { User } from '../users/user.entity';

export class Order {
    id: number;
    term: Date;
    comment: string;
    state: string;
    car: Car;
    client: User;
    employee: User;
    orderServices: OrderService[];
    messages: Message[];
    notifications: Notification[];
}
