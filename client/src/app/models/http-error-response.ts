import { HttpErrorResponse as HER } from "@angular/common/http";

export class HttpErrorResponse<T> extends HER {
    error: T
}