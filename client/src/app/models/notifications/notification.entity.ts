import { Order } from '../orders/order.entity';
import { User } from '../users/user.entity';

export class Notification {
    id: number;
    message: string;
    createdAt: Date;
    readAt: Date;
    user: User;
    order: Order;
}
