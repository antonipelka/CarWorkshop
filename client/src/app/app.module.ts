import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import {
    AccordionModule,
    AlertModule,
    BsDropdownModule,
    ButtonsModule,
    CollapseModule,
    ModalModule,
    TabsModule,
    TypeaheadModule
} from 'ngx-bootstrap';
import { AppComponent } from './app.component';
import { CheckOrderStatusComponent } from './check-order-status/check-order-status.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { LoginComponent } from './login/login.component';
import { MainPageComponent } from './main-page/main-page.component';
import { RegistrationComponent } from './registration/registration.component';
import { OrderStatusResolver } from './resolvers/order-status-resolver.service';
import { SessionResolver } from './resolvers/session-resolver.service';
import { AuthenticationService } from './services/authentication.service';
import { EmojiService } from './services/emoji.service';
import { HttpService } from './services/http.service';
import { MessagesService } from './services/messages.service';
import { NotificationsService } from './services/notifications.service';
import { OrdersService } from './services/orders.service';
import { ServicesService } from './services/services.service';
import { SessionService } from './services/session.service';
import { UsersService } from './services/users.service';
import { SharedModule } from './shared.module';

const appRoutes: Routes = [
    {
        path: 'activate/:activationToken',
        component: MainPageComponent
    },
    {
        path: 'reset/:resetToken',
        component: MainPageComponent
    },
    {
        path: '',
        component: MainPageComponent,
        children: [
            {
                path: 'order-status/:id',
                component: CheckOrderStatusComponent,
                resolve: {
                    orderStatus: OrderStatusResolver
                }
            }
        ]
    }
];

@NgModule({
    declarations: [
        AppComponent,
        RegistrationComponent,
        LoginComponent,
        MainPageComponent,
        CheckOrderStatusComponent
    ],
    imports: [
        SharedModule,
        BrowserModule,
        FormsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),
        AccordionModule.forRoot(),
        AlertModule.forRoot(),
        BsDropdownModule.forRoot(),
        ButtonsModule.forRoot(),
        CollapseModule.forRoot(),
        ModalModule.forRoot(),
        TabsModule.forRoot(),
        TypeaheadModule.forRoot(),
        DashboardModule,
    ],
    providers: [
        AuthenticationService,
        SessionService,
        HttpService,
        NotificationsService,
        OrdersService,
        MessagesService,
        UsersService,
        ServicesService,
        EmojiService,
        SessionResolver,
        OrderStatusResolver,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
