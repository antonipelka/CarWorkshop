import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Order } from '../models/orders/order.entity';

@Component({
    selector: 'app-check-order-status',
    templateUrl: './check-order-status.component.html',
    styleUrls: ['./check-order-status.component.scss']
})
export class CheckOrderStatusComponent implements OnInit {
    order: Order;

    constructor(private route: ActivatedRoute) { }

    ngOnInit() {
        this.route.data.subscribe((data: {
            orderStatus: Order
        }) => {
            this.order = data.orderStatus;
        });
    }

}
