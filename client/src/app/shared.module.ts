import { NgModule } from '@angular/core';
import { ValidationErrorPipe } from './pipes/validation-error.pipe';
import { OrderStatePipe } from './pipes/order-state.pipe';
import { HelpComponent } from './help/help.component';

@NgModule({
    declarations: [
        ValidationErrorPipe,
        OrderStatePipe,
        HelpComponent,
    ],
    imports: [],
    providers: [],
    exports: [
        ValidationErrorPipe,
        OrderStatePipe,
        HelpComponent,
    ]
})
export class SharedModule { }
