import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Car } from 'app/models/cars/car.entity';
import { CreateOrderDto } from 'app/dtos/orders/create-order.dto';
import { IMyDpOptions, IMyDate, IMyDateModel } from 'mydatepicker';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { CreateCarModalComponent } from '../cars/create-car-modal/create-car-modal.component';
import { Service } from 'app/models/services/service.entity';
import { OrdersService } from '../../services/orders.service';
import { ReservedTermsDto } from 'app/dtos/orders/reserved-terms.dto';

const WeekDayShorts = ['su', 'mo', 'tu', 'we', 'th', 'fr', 'sa'];
@Component({
    selector: 'app-create-order',
    templateUrl: './create-order.component.html',
    styleUrls: ['./create-order.component.scss']
})
export class CreateOrderComponent implements OnInit {
    errors: any[] = [];
    model: CreateOrderDto = new CreateOrderDto();

    cars: Car[] = [];
    carModalRef: BsModalRef;

    private services: Service[] = [];
    private _selectedServiceId: number = void 0;
    get selectedServiceId() {
        return this._selectedServiceId;
    }
    set selectedServiceId(newValue) {
        this._selectedServiceId = newValue ? +newValue : newValue;
    }

    termDatePickerOptions: IMyDpOptions = {
        dateFormat: 'yyyy-mm-dd',
        disableUntil: this.getTodayDate(),
        showTodayBtn: false,
        editableDateField: false,
        openSelectorOnInputClick: true,
    };

    reservedTerms: ReservedTermsDto;
    groupedSchedule: { [key in string]: number[] };
    hours: number[] = [];
    selectedDate: Date;
    selectedHour: number;

    isMeridian = false;
    readonly = true;
    myTime = new Date();

    bsValue = new Date();
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private modalService: BsModalService,
        private ordersService: OrdersService
    ) { }

    ngOnInit() {
        this.route.data.subscribe((data: {
            cars: Car[],
            services: Service[],
            reservedTerms: ReservedTermsDto
        }) => {
            this.cars = data.cars;
            this.services = data.services;
            this.reservedTerms = data.reservedTerms;
            this.updateDatePicker();
        });
    }

    private getTodayDate() {
        const date = new Date();
        const myDate: IMyDate = {
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate() - 1
        };
        return myDate;
    }

    private updateDatePicker() {
        this.groupedSchedule = this.reservedTerms.schedules.reduce<any>((grouped: Object, schedule) => {
            const dayShort = schedule.weekDay.substr(0, 2);
            if (grouped.hasOwnProperty(dayShort)) {
                grouped[dayShort].push(schedule.hour);
            } else {
                grouped[dayShort] = [schedule.hour];
            }
            return grouped;
        }, {});
        const availableDayShorts = Object.keys(this.groupedSchedule);
        const blockedDays = WeekDayShorts.filter(short => {
            return !availableDayShorts.includes(short);
        });
        this.termDatePickerOptions.disableWeekdays = blockedDays;
    }

    onDateChanged(event: IMyDateModel) {
        if (event.jsdate) {
            this.selectedDate = event.jsdate;
            const dayShort = WeekDayShorts[this.selectedDate.getDay()];
            this.hours = this.groupedSchedule[dayShort];
        } else {
            this.hours = [];
        }
    }

    isHourDisabled(hour: number) {
        const dateCopy = new Date(this.selectedDate);
        dateCopy.setHours(hour, 0, 0, 0);
        return this.reservedTerms.terms.map(partialOrder => partialOrder.term as any).includes(dateCopy.toISOString());
    }

    openCreateCarModal() {
        this.carModalRef = this.modalService.show(CreateCarModalComponent);
        this.carModalRef.content.onClose.subscribe(car => {
            this.cars.push(car);
            this.model.carId = car.id;
            this.carModalRef.content.onClose.unsubscribe();
        });
    }

    addSelectedService() {
        this.model.serviceIds.push(this.selectedServiceId);
        this.selectedServiceId = void 0;
    }

    removeService(index: number) {
        this.model.serviceIds.splice(index, 1);
    }

    availableServices() {
        return this.services.filter(service => this.model.serviceIds.indexOf(service.id) < 0);
    }

    getService(serviceId: number) {
        return this.services.find(service => service.id === serviceId);
    }

    submitOrder() {
        const selectedDate = new Date(this.selectedDate);
        selectedDate.setHours(+this.selectedHour, 0, 0, 0);
        this.model.term = selectedDate;

        this.ordersService.createOrder(this.model).subscribe(order => {
            this.router.navigate(['/dashboard/orders', order.id]);
        }, errors => {
            this.errors = errors;
        });
    }
}
