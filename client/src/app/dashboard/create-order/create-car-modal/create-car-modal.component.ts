import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { BsModalRef } from 'ngx-bootstrap';
import { CarsService } from '../../../services/cars.service';
import { CreateCarDto } from 'app/dtos/cars/create-car.dto';

@Component({
    selector: 'app-create-car-modal',
    templateUrl: './create-car-modal.component.html'
})
export class CreateCarModalComponent implements OnInit {
    public onClose: Subject<any>;
    errors: any[] = [];

    model: CreateCarDto = new CreateCarDto();

    constructor(private bsModalRef: BsModalRef, private carsService: CarsService) { }

    public ngOnInit() {
        this.onClose = new Subject();
    }

    public createCar() {
        this.carsService.createCar(this.model).subscribe(car => {
            this.onClose.next(car);
            this.close();
        }, ({ error }) => {
            this.errors = error.message;
        });
    }

    public close() {
        this.bsModalRef.hide();
    }
}
