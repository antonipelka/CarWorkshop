import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap';
import { BsModalService } from 'ngx-bootstrap/modal';
import { CreatePartDto } from '../../dtos/parts/create-part.dto';
import { UpdatePartDto } from '../../dtos/parts/update-part.dto';
import { Part } from '../../models/parts/part.entity';
import { PartsService } from '../../services/parts.service';

@Component({
    selector: 'app-car-parts',
    templateUrl: './car-parts.component.html',
    styleUrls: ['./car-parts.component.scss']
})
export class CarPartsComponent implements OnInit {
    parts: Part[] = [];
    selectedPart: Part;
    createModel: CreatePartDto = new CreatePartDto();
    updateModel: UpdatePartDto;

    newPartModal: BsModalRef;
    editPartModal: BsModalRef;
    deletePartModal: BsModalRef;

    constructor(
        private route: ActivatedRoute,
        private modalService: BsModalService,
        private partsService: PartsService
    ) { }

    ngOnInit() {
        this.route.data.subscribe((data: { parts: Part[] }) => {
            this.parts = data.parts;
        });
    }

    openNewPartModal(template: TemplateRef<any>) {
        this.newPartModal = this.modalService.show(template);
    }

    openEditPartModal(template: TemplateRef<any>, part: Part) {
        this.selectedPart = part;
        this.updateModel = new UpdatePartDto(part);
        this.editPartModal = this.modalService.show(template);
    }

    openDeletePartModal(template: TemplateRef<any>, part: Part) {
        this.selectedPart = part;
        this.deletePartModal = this.modalService.show(template);
    }

    createPart() {
        this.partsService.createPart(this.createModel).subscribe((part) => {
            this.parts.push(part);
            this.createModel = new CreatePartDto();
            this.newPartModal.hide();
        });
    }

    updatePart() {
        this.partsService.updatePart(this.selectedPart.id, this.updateModel).subscribe((part) => {
            const index = this.parts.indexOf(this.selectedPart);
            this.parts.splice(index, 1, part);
            this.editPartModal.hide();
        });
    }

    deletePart() {
        this.partsService.deletePart(this.selectedPart).subscribe((part) => {
            this.parts.splice(this.parts.indexOf(this.selectedPart), 1);
            this.deletePartModal.hide();
        });
    }


}
