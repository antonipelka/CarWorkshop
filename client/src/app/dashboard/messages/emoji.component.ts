export class Emoji {

    constructor(
        shortcut: String,
        value: String,
    ) {
        this.shortcut = shortcut;
        this.value = value;
    }

    public shortcut: String;
    public value: String;
}
