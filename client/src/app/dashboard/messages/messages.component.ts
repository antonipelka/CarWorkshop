import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Message } from 'app/models/messages/message.entity';
import { AuthenticationService } from '../../services/authentication.service';
import { EmojiService } from '../../services/emoji.service';
import { MessagesService } from '../../services/messages.service';

@Component({
    selector: 'app-messages',
    templateUrl: './messages.component.html',
    styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {
    messages: Message[] = [];
    userId: number;
    message = '';
    private orderId: number;

    constructor(
        private route: ActivatedRoute,
        private messagesService: MessagesService,
        private authentication: AuthenticationService,
        private emojiService: EmojiService
    ) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            if (params['id']) {
                this.orderId = +params['id'];
            }
        });
        this.route.data.subscribe((data: { messages: Message[] }) => {
            this.messages = data.messages;
        });
        this.userId = this.authentication.getLoggedUser().id;
    }

    sendMessage() {
        if (this.message.length) {
            this.message = this.emojiService.parseAfterSend(this.message);
            this.messagesService.sendMessage(this.orderId, { content: this.message }).subscribe(message => {
                this.message = '';
                this.messages.push(message);
            });
        }
    }

    onInputChange(text: String) {
        this.message = this.emojiService.parse(this.message);
    }

}
