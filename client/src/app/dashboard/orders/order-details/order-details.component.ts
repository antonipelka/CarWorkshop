import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Order } from 'app/models/orders/order.entity';
import { Service } from 'app/models/services/service.entity';
import { UpdateOrderDto } from '../../../dtos/orders/update-order.dto';
import { OrderServiceService } from '../../../services/order-service.service';
import { Car } from '../../../models/cars/car.entity';
import { OrdersService } from '../../../services/orders.service';

@Component({
    selector: 'app-order-details',
    templateUrl: './order-details.component.html',
    styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {
    order: Order;
    updateOrderDto: UpdateOrderDto;
    get isDirty() {
        const originalServiceIds = this.order.orderServices.map(orderService => orderService.service.id);
        return this.updateOrderDto.comment !== this.order.comment
            || this.updateOrderDto.carId !== this.order.car.id
            || this.updateOrderDto.state !== this.order.state
            || this.updateOrderDto.term !== this.order.term
            || JSON.stringify(this.updateOrderDto.serviceIds.concat().sort()) !== JSON.stringify(originalServiceIds.concat().sort());
    }
    private services: Service[] = [];
    private _selectedServiceId: number = void 0;
    get selectedServiceId() {
        return this._selectedServiceId;
    }
    set selectedServiceId(newValue) {
        this._selectedServiceId = newValue ? +newValue : newValue;
    }

    cars: Car[];

    states = ['created', 'accepted', 'in_progress', 'finished', 'rejected'];

    constructor(
        private route: ActivatedRoute,
        private readonly ordersService: OrdersService,
        private readonly orderServiceService: OrderServiceService,
    ) { }

    ngOnInit() {
        this.route.data.subscribe((data: { order: Order, services: Service[], cars: Car[] }) => {
            this.setOrder(data.order);
            this.services = data.services;
            this.cars = data.cars;
        });
    }

    private setOrder(order: Order) {
        this.order = order;
        this.updateOrderDto = new UpdateOrderDto(order);
    }

    availableServices() {
        return this.services.filter(service => !this.updateOrderDto.serviceIds.find(serviceId => serviceId === service.id));
    }

    addSelectedService() {
        this.updateOrderDto.serviceIds.push(this.selectedServiceId);
        this.selectedServiceId = void 0;
    }

    isCompleted(service: Service) {
        const orderService = this.order.orderServices.find(providedOrderService => providedOrderService.service.id === service.id);
        return orderService && orderService.isCompleted;
    }

    canToggleCompleted(serviceId: number) {
        return this.order.orderServices.find(orderService => orderService.service.id === serviceId);
    }

    toggleCompleted(serviceId: number) {
        const orderService = this.order.orderServices.find(providedOrderService => providedOrderService.service.id === serviceId);
        this.orderServiceService.changeOrderServiceCompletedState(orderService.id, !orderService.isCompleted)
            .subscribe(updatedOrderService => {
                orderService.isCompleted = updatedOrderService.isCompleted;
            });
    }

    removeService(index: number) {
        this.updateOrderDto.serviceIds.splice(index, 1);
    }

    updateOrder() {
        this.ordersService.updateOrder(this.order.id, this.updateOrderDto).subscribe(order => {
            this.setOrder(order);
        });
    }

    getService(serviceId: any): Service {
        return this.services.find(service => service.id === serviceId);
    }
}
