import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Order } from 'app/models/orders/order.entity';
import { AuthenticationService } from '../../services/authentication.service';
import { OrdersService } from '../../services/orders.service';

@Component({
    selector: 'app-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
    errors: any[] = [];
    orders: Order[] = [];

    constructor(
        protected ordersService: OrdersService,
        private route: ActivatedRoute,
    ) { }

    ngOnInit() {
        this.route.data.subscribe((data: {
            orders: Order[]
        }) => {
            this.orders = data.orders;
        });
    }

}
