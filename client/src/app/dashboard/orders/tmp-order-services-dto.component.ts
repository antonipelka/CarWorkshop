export class OrderService {

    constructor(
        id: Number,
        name: String,
        price: Number,
        description: String,
        isCompleted: Boolean
    ) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.isCompleted = isCompleted;
    }

    public id: Number;
    public name: String;
    public price: Number;
    public description: String;
    public isCompleted: Boolean;
}