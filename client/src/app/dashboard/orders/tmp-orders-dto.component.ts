export class Order {

    constructor(
        id: Number,
        term: Date,
        comment: String,
        state: String,
        carName: String,
        clientId: Number,
        clientName: String,
        employeeId: Number,
        services: Number[]
    ) {
        this.id = id;
        this.term = term;
        this.comment = comment;
        this.state = state;
        this.carName = carName;
        this.clientId = clientId;
        this.clientName = clientName;
        this.employeeId = employeeId;
        this.services = services;
    }

    public id: Number;
    public term: Date;
    public comment: String;
    public state: String;
    public carName: String;
    public clientId: Number;
    public clientName: String;
    public employeeId: Number;
    public services: Number[];
}