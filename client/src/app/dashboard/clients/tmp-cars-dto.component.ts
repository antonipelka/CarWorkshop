export class Car {

    constructor(
        id: Number,
        name: String,
    ) {
        this.id = id;
        this.name = name;
    }

    public id: Number;
    public name: String;
}