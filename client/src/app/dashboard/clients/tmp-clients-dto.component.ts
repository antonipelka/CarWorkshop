import { Car } from './tmp-cars-dto.component'

export class Client {

    constructor(
        id: Number,
        email: String,
        firstName: String,
        lastName: String,
        phoneNumber: String,
        latestOrder: String,
        date: Date,
        cars: Car[],
        orders: Number[],
    ) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.latestOrder = latestOrder;
        this.date = date;
        this.cars = cars;
        this.orders = orders;
    }

    public id: Number;
    public email: String;
    public firstName: String;
    public lastName: String;
    public phoneNumber: String;
    public latestOrder: String;
    public date: Date;
    public cars: Car[];
    public orders: Number[];
}