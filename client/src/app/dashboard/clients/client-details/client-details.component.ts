import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { User } from '../../../models/users/user.entity';
import { UpdateUserDto } from '../../../dtos/users/update-user.dto';
import { UsersService } from '../../../services/users.service';
import { CreateCarModalComponent } from '../../cars/create-car-modal/create-car-modal.component';
import { Car } from '../../../models/cars/car.entity';
import { CarsService } from '../../../services/cars.service';
import { Order } from '../../../models/orders/order.entity';
import { AuthenticationService } from '../../../services/authentication.service';

@Component({
    selector: 'app-client-details',
    templateUrl: './client-details.component.html',
    styleUrls: ['./client-details.component.scss']
})
export class ClientDetailsComponent implements OnInit {
    errors: any[] = [];
    client: User;
    cars: Car[] = [];
    orders: Order[] = [];
    clientModel: UpdateUserDto;

    editClientModal: BsModalRef;
    editCarModal: BsModalRef;
    removeCarModal: BsModalRef;
    removedCar: Car = null;
    addCarModal: BsModalRef;
    resetPasswordModal: BsModalRef;

    constructor(
        private modalService: BsModalService,
        private route: ActivatedRoute,
        private readonly usersService: UsersService,
        private readonly carsService: CarsService,
        private readonly authentication: AuthenticationService,
    ) { }

    ngOnInit() {
        this.route.data.subscribe((data: { client: User, cars: Car[], orders: Order[] }) => {
            this.client = data.client;
            this.cars = data.cars;
            this.orders = data.orders;
            this.clientModel = new UpdateUserDto(this.client);
        });
    }

    openEditClientModal(template: TemplateRef<any>) {
        this.editClientModal = this.modalService.show(template);
    }

    updateClient() {
        this.usersService.updateUser(this.client.id, this.clientModel).subscribe(client => {
            this.client = client;
            this.authentication.requestUser(true);
        }, errors => {
            this.errors = errors;
        });
        this.editClientModal.hide();
    }

    openEditCarModal(car: Car) {
        this.editCarModal = this.modalService.show(CreateCarModalComponent, {
            initialState: {
                car: car
            }
        });
        this.editCarModal.content.onClose.subscribe(updatedCar => {
            const index = this.cars.indexOf(car);
            this.cars[index] = updatedCar;
            this.editCarModal.content.onClose.unsubscribe();
        });
    }

    openRemoveCarModal(template: TemplateRef<any>, car: Car) {
        this.removeCarModal = this.modalService.show(template);
        this.removedCar = car;
    }

    removeCar() {
        this.carsService.deleteCar(this.removedCar).subscribe(() => {
            const index = this.cars.indexOf(this.removedCar);
            this.cars.splice(index, 1);
            this.removedCar = null;
            this.removeCarModal.hide();
        });
    }

    openAddCarModal() {
        this.addCarModal = this.modalService.show(CreateCarModalComponent, {
            initialState: {
                clientId: this.client.id
            }
        });
        this.addCarModal.content.onClose.subscribe(car => {
            this.cars.push(car);
            this.addCarModal.content.onClose.unsubscribe();
        });
    }

}
