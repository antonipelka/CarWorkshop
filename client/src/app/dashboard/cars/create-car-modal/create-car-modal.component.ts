import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { BsModalRef } from 'ngx-bootstrap';
import { CarsService } from '../../../services/cars.service';
import { CreateCarDto } from 'app/dtos/cars/create-car.dto';
import { Car } from '../../../models/cars/car.entity';
import { UpdateCarDto } from '../../../dtos/cars/update-car.dto';

@Component({
    selector: 'app-create-car-modal',
    templateUrl: './create-car-modal.component.html'
})
export class CreateCarModalComponent implements OnInit {
    public onClose: Subject<any>;
    errors: any[] = [];

    model: CreateCarDto | UpdateCarDto = new CreateCarDto();
    clientId: number = null;
    car: Car = null;

    constructor(private bsModalRef: BsModalRef, private carsService: CarsService) { }

    public ngOnInit() {
        this.onClose = new Subject();
        if (this.car) {
            this.model = new UpdateCarDto(this.car);
        } else if (this.clientId) {
            (this.model as CreateCarDto).userId = this.clientId;
        }
    }

    public submit() {
        this.car ? this.updateCar() : this.createCar();
    }

    public createCar() {
        this.carsService.createCar(this.model as CreateCarDto).subscribe(car => {
            this.onClose.next(car);
            this.close();
        }, errors => {
            this.errors = errors;
        });
    }

    public updateCar() {
        this.carsService.updateCar(this.car.id, this.model as UpdateCarDto).subscribe(car => {
            this.onClose.next(car);
            this.close();
        }, errors => {
            this.errors = errors;
        });
    }

    public close() {
        this.bsModalRef.hide();
    }
}
