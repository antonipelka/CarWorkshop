import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Service } from 'app/models/services/service.entity';
import { BsModalRef } from 'ngx-bootstrap';
import { BsModalService } from 'ngx-bootstrap/modal';
import { CreateServiceDto } from '../../dtos/services/create-service.dto';
import { ServicesService } from '../../services/services.service';
import { Part } from '../../models/parts/part.entity';
import { UpdateServiceDto } from '../../dtos/services/update-service.dto';

@Component({
    selector: 'app-services',
    templateUrl: './services.component.html',
    styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {
    errors = [];
    serviceModal: BsModalRef;
    services: Service[];
    parts: Part[];
    serviceModel: CreateServiceDto | UpdateServiceDto = new CreateServiceDto();
    private _selectedPartId: number = void 0;
    get selectedPartId() {
        return this._selectedPartId;
    }
    set selectedPartId(newValue) {
        this._selectedPartId = newValue ? +newValue : newValue;
    }

    get isEdit() {
        return this.serviceModel instanceof UpdateServiceDto;
    }

    editedService: Service;

    deleteServiceModal: BsModalRef;
    removedService: Service;

    constructor(
        private route: ActivatedRoute,
        private modalService: BsModalService,
        private servicesService: ServicesService
    ) { }

    ngOnInit() {
        this.route.data.subscribe((data: { services: Service[], parts: Part[] }) => {
            this.services = data.services;
            this.parts = data.parts;
        });
    }

    openNewServiceModal(template: TemplateRef<any>) {
        this.serviceModel = new CreateServiceDto();
        this.serviceModal = this.modalService.show(template);
    }

    addSelectedPart() {
        this.serviceModel.partIds.push(this.selectedPartId);
        this.selectedPartId = void 0;
    }

    availableParts() {
        return this.parts.filter(part => {
            return this.serviceModel.partIds.indexOf(part.id) < 0;
        });
    }

    getPart(id: number) {
        return this.parts.find(part => part.id === id);
    }

    removePart(index: number) {
        this.serviceModel.partIds.splice(index, 1);
    }

    addService() {
        this.servicesService.createService(this.serviceModel).subscribe(service => {
            this.services.push(service);
            this.serviceModal.hide();
            this.errors = [];
        }, errors => {
            this.errors = errors;
        });
    }

    openEditServiceModal(template: TemplateRef<any>, service: Service) {
        this.serviceModel = new UpdateServiceDto({
            name: service.name,
            description: service.description,
            isChoosable: service.isChoosable,
            price: service.price,
            partIds: service.parts.map(part => part.id),
        });
        this.editedService = service;
        this.serviceModal = this.modalService.show(template);
    }

    editService() {
        this.servicesService.updateService(this.editedService.id, this.serviceModel).subscribe(service => {
            const index = this.services.indexOf(this.editedService);
            this.services.splice(index, 1, service);
            this.serviceModal.hide();
        }, errors => {
            this.errors = errors;
        });
    }

    submit() {
        this.isEdit ? this.editService() : this.addService();
    }

    openDeleteServiceModal(template: TemplateRef<any>, service: Service) {
        this.removedService = service;
        this.deleteServiceModal = this.modalService.show(template);
    }

    deleteService() {
        this.servicesService.deleteService(this.removedService.id).subscribe(() => {
            const index = this.services.indexOf(this.removedService);
            this.services.splice(index, 1);
            this.deleteServiceModal.hide();
        });
    }

}
