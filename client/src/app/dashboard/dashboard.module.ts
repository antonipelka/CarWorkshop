import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { MyDatePickerModule } from 'mydatepicker';
import { AccordionModule, BsDropdownModule, CollapseModule, ModalModule, TimepickerModule } from 'ngx-bootstrap';
import { IsEmployeeDirective } from '../directives/is-employee.directive';
import { AuthGuard } from '../guards/auth.guard';
import { FullNamePipe } from '../pipes/full-name.pipe';
import { WeekDayPipe } from '../pipes/week-day.pipe';
import { CarsResolver } from '../resolvers/cars-resolver.service';
import { ClientResolver } from '../resolvers/client-resolver.service';
import { ClientsResolver } from '../resolvers/clients-resolver.service';
import { MessagesResolver } from '../resolvers/messages-resolver.service';
import { OrderResolver } from '../resolvers/order-resolver.service';
import { OrdersResolver } from '../resolvers/orders-resolver.service';
import { PartsResolver } from '../resolvers/parts-resolver.service';
import { ReservedTermsResolver } from '../resolvers/reserved-terms-resolver.service';
import { SchedulesResolver } from '../resolvers/schedules-resolver.service';
import { ServicesResolver } from '../resolvers/services-resolver.service';
import { SessionResolver } from '../resolvers/session-resolver.service';
import { CarsService } from '../services/cars.service';
import { OrderServiceService } from '../services/order-service.service';
import { PartsService } from '../services/parts.service';
import { SchedulesService } from '../services/schedules.service';
import { ServicesService } from '../services/services.service';
import { SharedModule } from '../shared.module';
import { CarPartsComponent } from './car-parts/car-parts.component';
import { CreateCarModalComponent } from './cars/create-car-modal/create-car-modal.component';
import { ClientDetailsComponent } from './clients/client-details/client-details.component';
import { ClientsComponent } from './clients/clients.component';
import { CreateOrderComponent } from './create-order/create-order.component';
import { DashboardComponent } from './dashboard.component';
import { MessagesComponent } from './messages/messages.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { OrderDetailsComponent } from './orders/order-details/order-details.component';
import { OrdersComponent } from './orders/orders.component';
import { SchedulesComponent } from './schedules/schedules.component';
import { ServicesComponent } from './services/services.component';
import { NotificationsResolver } from '../resolvers/notifications-resolver.service';

const dashboardRoutes: Routes = [
    {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard],
        resolve: {
            session: SessionResolver,
        },
        children: [
            {
                path: '',
                redirectTo: 'orders',
                pathMatch: 'full',
            },
            {
                path: 'book',
                component: CreateOrderComponent,
                resolve: {
                    cars: CarsResolver,
                    services: ServicesResolver,
                    reservedTerms: ReservedTermsResolver,
                }
            },
            {
                path: 'orders/:id',
                component: OrderDetailsComponent,
                resolve: {
                    order: OrderResolver,
                    messages: MessagesResolver,
                    services: ServicesResolver,
                    cars: CarsResolver,
                },
                data: {
                    allServices: true,
                    carsByOrderClient: true,
                }
            },
            {
                path: 'orders',
                component: OrdersComponent,
                resolve: {
                    orders: OrdersResolver,
                }
            },
            {
                path: 'parts',
                component: CarPartsComponent,
                resolve: {
                    parts: PartsResolver,
                }
            },
            {
                path: 'clients',
                component: ClientsComponent,
                resolve: {
                    clients: ClientsResolver,
                }
            },
            {
                path: 'clients/:id',
                component: ClientDetailsComponent,
                resolve: {
                    client: ClientResolver,
                    cars: CarsResolver,
                    orders: OrdersResolver,
                }
            },
            {
                path: 'services',
                component: ServicesComponent,
                resolve: {
                    services: ServicesResolver,
                    parts: PartsResolver,
                }
            },
            {
                path: 'notifications',
                component: NotificationsComponent,
                resolve: {
                    notifications: NotificationsResolver,
                }
            },
            {
                path: 'profile',
                component: ClientDetailsComponent,
                resolve: {
                    client: ClientResolver,
                }
            },
            {
                path: 'schedules',
                component: SchedulesComponent,
                resolve: {
                    schedules: SchedulesResolver,
                }
            },
        ]
    },
];

@NgModule({
    imports: [
        SharedModule,
        CommonModule,
        FormsModule,
        RouterModule.forChild(dashboardRoutes),
        AccordionModule.forRoot(),
        BsDropdownModule.forRoot(),
        CollapseModule.forRoot(),
        TimepickerModule.forRoot(),
        ModalModule.forRoot(),
        MyDatePickerModule,
    ],
    declarations: [
        DashboardComponent,
        NavigationComponent,
        OrdersComponent,
        OrderDetailsComponent,
        IsEmployeeDirective,
        CreateOrderComponent,
        ClientsComponent,
        ClientDetailsComponent,
        ServicesComponent,
        NotificationsComponent,
        MessagesComponent,
        CreateCarModalComponent,
        FullNamePipe,
        SchedulesComponent,
        CarPartsComponent,
        WeekDayPipe,
    ],
    providers: [
        AuthGuard,
        CarsResolver,
        CarsService,
        ServicesResolver,
        ServicesService,
        ReservedTermsResolver,
        OrdersResolver,
        OrderResolver,
        MessagesResolver,
        ClientsResolver,
        ClientResolver,
        SchedulesResolver,
        SchedulesService,
        PartsService,
        PartsResolver,
        OrderServiceService,
        NotificationsResolver,
    ],
    entryComponents: [
        CreateCarModalComponent,
    ]
})
export class DashboardModule { }
