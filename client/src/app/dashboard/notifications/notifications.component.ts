import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Notification } from '../../models/notifications/notification.entity';

@Component({
    selector: 'app-notifications',
    templateUrl: './notifications.component.html',
    styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
    notifications: Notification[];

    constructor(private route: ActivatedRoute) { }

    ngOnInit() {
        this.route.data.subscribe((data: { notifications: Notification[] }) => {
            this.notifications = data.notifications;
        });
    }
}
