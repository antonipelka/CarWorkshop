export class Notification {

    constructor(
        id: Number,
        message: String,
        createdAt: Date,
        readAt: Date,
        orderId: Number
    ) {
        this.id = id;
        this.message = message;
        this.createdAt = createdAt;
        this.readAt = readAt;
        this.orderId = orderId;
    }

    public id: Number;
    public message: String;
    public createdAt: Date;
    public readAt: Date;
    public orderId: Number;
}