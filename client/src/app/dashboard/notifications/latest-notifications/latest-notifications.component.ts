import { Component, OnInit } from '@angular/core';

import { NotificationsService } from '../../../services/notifications.service';
import { DateFormatter } from '../../../services/date-formatter.service';

@Component({
  selector: 'app-latest-notifications',
  templateUrl: './latest-notifications.component.html',
  styleUrls: ['./latest-notifications.component.scss']
})
export class LatestNotificationsComponent implements OnInit {

  formatDate = DateFormatter.formatDate;
  formatTime = DateFormatter.formatTime;

  constructor(private notificationsService: NotificationsService) {
  }

  ngOnInit() {
  }

  canNotificationBeDisplayed(orderId: Number): Boolean {
    if (orderId == 5 || orderId == 24)
      return true;

    return false;
    // TODO: odkomentować poniżej i usunąć powyżej, jak będzie obsłużone order[0].employee

    // var user = this.authenticationService.getLoggedUser();
    // var order = this.ordersService.getOrder(orderId);
    // if (user.isEmployee) {
    //   if (order.employee.id == user.id) {
    //     return true;
    //   }
    // } else {
    //   if (order.client.id == user.id) {
    //     return true;
    //   }
    // }
    // return false;
  }

}
