import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LatestNotificationsComponent } from './latest-notifications.component';

describe('LatestNotificationsComponent', () => {
  let component: LatestNotificationsComponent;
  let fixture: ComponentFixture<LatestNotificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LatestNotificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LatestNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
