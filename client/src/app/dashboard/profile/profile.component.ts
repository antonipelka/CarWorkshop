import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  private email;
  private firstName
  private lastName;
  private address;
  private phoneNumber;
  private identificationNumber;

  constructor(
    private authenticationService: AuthenticationService
  ) {
    var user = authenticationService.getLoggedUser();

    this.email = "AuthenticatedUserDto nie posiada pola email";
    this.firstName = user.firstName;
    this.lastName = user.lastName;
    this.address = "AuthenticatedUserDto nie posiada pola address";
    this.phoneNumber = "AuthenticatedUserDto nie posiada pola number";
    this.identificationNumber = "AuthenticatedUserDto nie posiada pola identificationNumber";
  }

  ngOnInit() {
  }

  saveChanges() {
    // TODO: zaimplementować
  }

}
