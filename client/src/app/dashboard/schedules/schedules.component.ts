import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Schedule } from '../../models/schedules/schedule.entity';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { SchedulesService } from '../../services/schedules.service';
import { CreateScheduleDto } from '../../dtos/schedules/create-schedule.dto';

@Component({
    selector: 'app-schedules',
    templateUrl: './schedules.component.html',
    styleUrls: ['./schedules.component.scss']
})
export class SchedulesComponent implements OnInit {
    weekDays = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
    schedules: Schedule[] = [];
    newScheduleModal: BsModalRef;
    deleteScheduleModal: BsModalRef;
    model: CreateScheduleDto = new CreateScheduleDto();
    removedSchedule: Schedule;

    constructor(private route: ActivatedRoute, private modalService: BsModalService, private scheduleSerivce: SchedulesService) { }

    ngOnInit() {
        this.route.data.subscribe((data: { schedules: Schedule[] }) => {
            this.schedules = data.schedules;
        });
    }

    openNewScheduleModal(template: TemplateRef<any>) {
        this.newScheduleModal = this.modalService.show(template);
    }

    openDeleteScheduleModal(template: TemplateRef<any>, schedule: Schedule) {
        this.deleteScheduleModal = this.modalService.show(template);
        this.removedSchedule = schedule;
    }

    createSchedule() {
        this.scheduleSerivce.createSchedule(this.model).subscribe((schedule) => {
            this.schedules.push(schedule);
            this.model = new CreateScheduleDto();
        });
    }

    deleteSchedule() {
        this.scheduleSerivce.deleteSchedule(this.removedSchedule).subscribe((schedule) => {
            this.schedules.splice(this.schedules.indexOf(this.removedSchedule), 1);
        });
    }
}
