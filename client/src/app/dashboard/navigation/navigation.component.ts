import { Component, OnInit } from '@angular/core';
import { AuthenticatedUserDto } from 'app/dtos/auth/authenticated-user.dto';
import { AuthenticationService } from '../../services/authentication.service';
import { NotificationsService } from '../../services/notifications.service';
import { OrdersService } from '../../services/orders.service';

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
    isCollapsed = true;
    user: AuthenticatedUserDto = null;
    orderCount: number;
    unreadCount: number;

    constructor(
        private authentication: AuthenticationService,
        protected notificationsService: NotificationsService,
        protected ordersService: OrdersService) { }

    ngOnInit() {
        this.authentication.getLoggedUserSubject().subscribe(loggedUser => {
            this.user = loggedUser;
        });
        this.authentication.requestUser();

        this.ordersService.getOrdersCount().subscribe(orderCount => {
            this.orderCount = orderCount;
        });

        this.notificationsService.getUnreadCount().subscribe(unreadCount => {
            this.unreadCount = unreadCount;
        });
    }

    logOut(event: MouseEvent) {
        this.authentication.logOut();
        event.preventDefault();
    }

}
