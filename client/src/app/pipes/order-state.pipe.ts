import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'orderState'
})
export class OrderStatePipe implements PipeTransform {
    private map = {
        created: 'Złożone',
        accepted: 'Przyjęte',
        in_progress: 'W realizacji',
        finished: 'Ukończone',
        rejected: 'Odrzucone',
    };

    transform(orderState: string): string {
        return this.map[orderState] || 'Nieznany';
    }

}
