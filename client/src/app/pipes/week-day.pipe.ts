import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'weekDay'
})
export class WeekDayPipe implements PipeTransform {
    private map = {
        monday: 'Poniedziałek',
        tuesday: 'Wtorek',
        wednesday: 'Środa',
        thursday: 'Czwartek',
        friday: 'Piątek',
        saturday: 'Sobota',
        sunday: 'Niedziela',
    };

    transform(weekDay: string): string {
        return this.map[weekDay];
    }

}
