import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../models/users/user.entity';

@Pipe({
    name: 'fullName'
})
export class FullNamePipe implements PipeTransform {

    transform(user: User): string {
        return `${user.firstName} ${user.lastName}`;
    }

}
