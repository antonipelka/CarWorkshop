import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'validationError' })
export class ValidationErrorPipe implements PipeTransform {
    transform(validationError: any) {
        const errors = [];
        for (const constraint in validationError.constraints) {
            if (validationError.constraints.hasOwnProperty(constraint)) {
                errors.push(validationError.constraints[constraint]);
            }
        }

        return errors;
    }
}
