// tslint:disable:max-line-length
import { MainPageComponent } from '../main-page/main-page.component';
import { CheckOrderStatusComponent } from '../check-order-status/check-order-status.component';
import { OrdersComponent } from '../dashboard/orders/orders.component';
import { OrderDetailsComponent } from '../dashboard/orders/order-details/order-details.component';
import { CreateOrderComponent } from '../dashboard/create-order/create-order.component';
import { NotificationsComponent } from '../dashboard/notifications/notifications.component';
import { ClientDetailsComponent } from '../dashboard/clients/client-details/client-details.component';
import { CarPartsComponent } from '../dashboard/car-parts/car-parts.component';
import { ClientsComponent } from '../dashboard/clients/clients.component';
import { ServicesComponent } from '../dashboard/services/services.component';
import { SchedulesComponent } from '../dashboard/schedules/schedules.component';

export const HelpMessages = [
    {
        component: MainPageComponent,
        message: `Aby sprawdzić status zlecenia należy nacisnąć zakładkę „Sprawdź status zlecenia”.
        Następnie w puste pole tekstowe należy wpisać numer zlecenia którego status chcemy sprawdzić, a następnie kliknąć przycisk „Sprawdź”. Po dokonaniu wyżej wymienionych informacji na ekranie wyświetli się status zlecenia realizowanego przez warsztat.
        W celu skorzystania z pełnych możliwości serwisu kliknij przycisk „Zarejestruj się”. Następnie uzupełnij formularz prawdziwymi danymi i zatwierdź. Na podany w formularzu adres e-mail wysłany zostanie link aktywacyjny do konta.
        Po zarejestrowaniu kliknij opcję „Zaloguj” by korzystać ze wszystkich opcji serwisu. Do logowania na konto posłużą podany w formularzu adres e-mail oraz hasło. Jeśli nie pamiętasz hasła kliknij „Nie pamiętam hasła” i postępuj według dalszych instrukcji.`
    },
    {
        component: CheckOrderStatusComponent,
        message: `Aby sprawdzić status zlecenia w puste pole wpisz jego numer i zatwierdź przyciskiem "Sprawdź". Następnie zostaniesz przekierowany do strony, na której widoczny będzie status sprawdzanego zlecenia.`
    },
    {
        component: NotificationsComponent,
        message: `W zakładce „Powiadomienia” widzimy wiadomości wysyłane przez warsztat – aktualizacje oraz odpowiedzi na zadawane przez nas pytania. Po wybraniu danego powiadomienia zostajemy przekierowani do strony, której ono dotyczy.`
    },
];

export const HelpMessagesClient = [
    {
        component: OrdersComponent, message: `W zakładce „Zlecenia” znajdują się bieżące zlecenia klienta, ich status oraz data w którym zostało przyjęte. Aby sprawdzić szczegóły zlecenia wybierz je z listy.
    Wysyłanie wiadomości do serwisu możliwe jest bezpośrednio po przejściu do konkretnego zlecenia.`
    },
    {
        component: OrderDetailsComponent, message: `W celu sprawdzenia szczegółów zlecenia, wybierz je z listy. Następnie na ekranie pojawią się informacje takie jak rodzaj zlecenia, usługi, data oraz inne szczegóły zlecenia.`
    },
    {
        component: CreateOrderComponent, message: `Zakładka „Umów wizytę” daje możliwość umówienia się na wizytę w warsztacie na określoną godzinę i datę. Z listy rozwijanej należy wybrać markę samochodu oraz usługę jaka nas interesuje.
    W pole „Uwagi” można dopisać dodatkowe informacje odnośnie danej wizyty mające usprawnić jej przebieg.`
    },
    {
        component: ClientDetailsComponent, message: `W zakładce możliwe jest obserwowanie danych klienta wpisanych przy rejestracji, historia zleceń. Poprzez kliknięcie przycisku "Edytuj" możliwa jest zmiana wprowadzonych wcześniej danych.`
    },
];

export const HelpMessagesEmployee = [
    {
        component: OrdersComponent, message: `W zakładce „Zlecenia” znajdują się bieżące zlecenia klientów, ich status oraz data w którym zostało przyjęte. W celu sprawdzenia szczegółów zlecenia, bądź je zaktualizować należy wybrać je z listy.`
    },
    {
        component: OrderDetailsComponent, message: `W tym miejscu dostępne są szczegóły zlecenia. Po prawej stronie dostępny jest czat w którym wyświetlane są wiadomości przypisane do danego zlecenia.
    Zmiana statusu zlecenia możliwa jest po wybraniu odpowiedniej opcji z listy rozwijanej.
    Usługi przypisane do danego zlecenia wyświetlane są w polu „Usługi”. Dodatkowe dane wyświetlane w zakładce odnośnie zlecenia to data jego przyjęcia oraz marka auta.`
    },
    {
        component: ClientDetailsComponent, message: `W zakładce możliwe jest sprawdzenie danych klienta wpisanych przy rejestracji, historia zleceń. Poprzez kliknięcie przycisku "Edytuj" możliwa jest zmiana wprowadzonych wcześniej danych.`
    },
    {
        component: CarPartsComponent, message: `Zakładka „Części” pozwala zarządzać bazą części. Na ekranie zostanie wyświetlona lista części wraz z ich liczbą oraz przyciski odpowiedzialne za edycję, dodawanie i usuwanie części.
    W celu dodania nowej części kliknij przycisk „Dodaj”, uzupełnij formularz i zatwierdź.
    Edycja części odbywa się po wybraniu opcji „Edytuj”, podaj ilość oraz identyfikator części i zatwierdź.
    Usuwanie części realizowane jest po kliknięciu przycisku „Usuń”.`
    },
    {
        component: ClientsComponent, message: `Zakładka „Klienci” zawiera tabelę wszystkich klientów zakładu przypisanych do konkretnych mechaników. Możliwa jest edycja i usuwanie klienta po wybraniu go z listy.
    Dodatkowo istnieje możliwość obserwacji wszystkich zleceń danego klienta, oraz edycja, dodawanie i usuwanie samochodów przypisanych do klienta po kliknięciu odpowiedniego przycisku.`
    },
    {
        component: ServicesComponent, message: `W zakładce „Usługi” znajdują się wszystkie usługi oferowane przez warsztat. Możliwa jest również ich edycja, usuwanie i dodawanie z poziomu pracownika.`
    },
    {
        component: SchedulesComponent, message: `Aby wprowadzić sprawdzić lub wprowadzić nowy termin do terminarza kliknij przycisk "Dodaj termin", następnie uzupełnij puste pola i zatwierdź operację. W celu usunięcie terminu z listy naciśnij przycisk "Usuń".`
    }
];
