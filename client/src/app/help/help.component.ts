import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { HelpMessages, HelpMessagesClient, HelpMessagesEmployee } from './help-messages';
import { SessionService } from '../services/session.service';

@Component({
    selector: 'app-help',
    templateUrl: './help.component.html',
    styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {
    modalRef: BsModalRef;
    helpMessage = 'Hmm...';
    constructor(
        private modalService: BsModalService,
        public route: ActivatedRoute,
        private sessionService: SessionService,
    ) { }

    private getDeepestComponent(activatedRoute: ActivatedRoute) {
        if (activatedRoute.children.length) {
            return this.getDeepestComponent(activatedRoute.children[0]);
        }
        return activatedRoute.component;
    }

    private getHelpMessage() {
        const component = this.getDeepestComponent(this.route);
        const helpObject = HelpMessages.find(help => help.component === component)
            || (this.sessionService.isEmployee()
                ? HelpMessagesEmployee.find(help => help.component === component)
                : HelpMessagesClient.find(help => help.component === component));
        return helpObject ? helpObject.message : 'Ten widok nie posiada pomocy kontekstowej.';
    }

    openModal(template: TemplateRef<any>) {
        this.helpMessage = this.getHelpMessage();
        this.modalRef = this.modalService.show(template);
    }

    ngOnInit() { }

}
