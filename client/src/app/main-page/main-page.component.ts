import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ResetPasswordDto } from '../dtos/auth/reset-password.dto';
import { AuthenticationService } from '../services/authentication.service';
import { UsersService } from '../services/users.service';

@Component({
    selector: 'app-main-page',
    templateUrl: './main-page.component.html',
    styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {
    errors: any[] = [];
    isActivated = false;
    isCollapsed = true;
    orderId: number;
    statusModal: BsModalRef;
    resetPasswordModal: BsModalRef;
    resetPasswordModel = new ResetPasswordDto;
    @ViewChild('resetPassword') resetPasswordTemplate: TemplateRef<any>;

    constructor(
        private modalService: BsModalService,
        private route: ActivatedRoute,
        private usersService: UsersService,
        private authenticationService: AuthenticationService
    ) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            const code = params['activationToken'];
            if (code) {
                this.usersService.activate(code).subscribe(() => {
                    this.isActivated = true;
                });
            }
            const resetToken = params['resetToken'];
            if (resetToken) {
                this.resetPasswordModel.code = resetToken;
                setTimeout(() => this.openResetPasswordModal(this.resetPasswordTemplate));
            }
        });
    }

    closeResetPasswordModal() {
        this.resetPasswordModel = new ResetPasswordDto;
        this.errors = [];
        this.resetPasswordModal.hide();
        this.resetPasswordModal = null;
    }

    async resetPasswordSubmit() {
        this.authenticationService.updatePassword(this.resetPasswordModel).subscribe(() => {
            this.closeResetPasswordModal();
        });
    }

    openResetPasswordModal(template: TemplateRef<any>) {
        this.resetPasswordModal = this.modalService.show(template, { class: 'modal-sm' });
    }

    openStatusModal(template: TemplateRef<any>) {
        this.statusModal = this.modalService.show(template, { class: 'modal-sm' });
    }
}
