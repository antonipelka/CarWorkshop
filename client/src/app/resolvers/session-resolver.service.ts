import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from '../services/authentication.service';
import { AuthenticatedUserDto } from 'app/dtos/auth/authenticated-user.dto';

@Injectable()
export class SessionResolver implements Resolve<AuthenticatedUserDto> {
    constructor(private authenticationService: AuthenticationService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<AuthenticatedUserDto> {
        return this.authenticationService.preSignIn();
    }
}
