import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Part } from '../models/parts/part.entity';
import { PartsService } from '../services/parts.service';


@Injectable()
export class PartsResolver implements Resolve<Part[]> {
    constructor(private partsService: PartsService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Part[]> {
        return this.partsService.getParts();
    }
}
