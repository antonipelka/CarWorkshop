import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Order } from 'app/models/orders/order.entity';
import { OrdersService } from '../services/orders.service';

@Injectable()
export class OrderStatusResolver implements Resolve<Order | boolean> {
    constructor(private ordersService: OrdersService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Order | boolean> {
        return this.ordersService.getOrderStatus(route.params.id).catch(() => Observable.of(null));
    }
}
