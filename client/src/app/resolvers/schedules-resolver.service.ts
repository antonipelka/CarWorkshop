import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Schedule } from '../models/schedules/schedule.entity';
import { SchedulesService } from '../services/schedules.service';


@Injectable()
export class SchedulesResolver implements Resolve<Schedule[]> {
    constructor(private schedulesService: SchedulesService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Schedule[]> {
        return this.schedulesService.getSchedules();
    }
}
