import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Service } from 'app/models/services/service.entity';
import { ServicesService } from '../services/services.service';

@Injectable()
export class ServicesResolver implements Resolve<Service[]> {
    constructor(private servicesService: ServicesService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Service[]> {
        return this.servicesService.getServices(route.data.allServices);
    }
}
