import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { User } from '../models/users/user.entity';
import { UsersService } from '../services/users.service';

@Injectable()
export class ClientResolver implements Resolve<User> {
    constructor(private usersService: UsersService) { }

    resolve(route: ActivatedRouteSnapshot) {
        return route.params.id ? this.usersService.getClient(route.params.id) : this.usersService.getUserProfile();
    }
}
