import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Notification } from '../models/notifications/notification.entity';
import { NotificationsService } from '../services/notifications.service';

@Injectable()
export class NotificationsResolver implements Resolve<Notification[]> {
    constructor(private notificationsService: NotificationsService) { }

    resolve(): Observable<Notification[]> {
        return this.notificationsService.getNotifications();
    }
}
