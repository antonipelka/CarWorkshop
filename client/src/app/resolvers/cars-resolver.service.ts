import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Car } from 'app/models/cars/car.entity';
import { CarsService } from '../services/cars.service';

@Injectable()
export class CarsResolver implements Resolve<Car[]> {
    constructor(private carsService: CarsService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Car[]> {
        return route.data.carsByOrderClient
            ? this.carsService.getCarsByOrderClient(route.params.id)
            : route.params.id
                ? this.carsService.getCarsByClient(route.params.id)
                : this.carsService.getCars();
    }
}
