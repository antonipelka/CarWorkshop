import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Order } from 'app/models/orders/order.entity';
import { OrdersService } from '../services/orders.service';


@Injectable()
export class OrdersResolver implements Resolve<Order[]> {
    constructor(private ordersService: OrdersService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Order[]> {
        return route.params.id ? this.ordersService.getClientOrders(route.params.id) : this.ordersService.getOrders();
    }
}
