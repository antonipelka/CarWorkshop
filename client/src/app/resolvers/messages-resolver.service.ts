import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Message } from 'app/models/messages/message.entity';
import { Observable } from 'rxjs/Observable';
import { MessagesService } from '../services/messages.service';


@Injectable()
export class MessagesResolver implements Resolve<Message[]> {
    constructor(private messagesService: MessagesService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Message[]> {
        return this.messagesService.getOrderMessages(route.params.id);
    }
}
