import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UsersService } from '../services/users.service';
import { User } from '../models/users/user.entity';

@Injectable()
export class ClientsResolver implements Resolve<User[]> {
    constructor(private usersService: UsersService) { }

    resolve(): Observable<User[]> {
        return this.usersService.getClients();
    }
}
