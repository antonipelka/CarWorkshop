import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { OrdersService } from '../services/orders.service';
import { ReservedTermsDto } from 'app/dtos/orders/reserved-terms.dto';

@Injectable()
export class ReservedTermsResolver implements Resolve<ReservedTermsDto> {
    constructor(private ordersService: OrdersService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ReservedTermsDto> {
        return this.ordersService.getReservedTerms();
    }
}
