import { Component, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { CreateUserDto } from 'app/dtos/users/create-user.dto';
import { UsersService } from '../services/users.service';

@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent {

    model = new CreateUserDto();
    errors: any[] = [];

    registrationModal: BsModalRef;

    constructor(private modalService: BsModalService, private userService: UsersService) { }

    openRegistrationModal(template: TemplateRef<any>) {
        this.registrationModal = this.modalService.show(template, { class: 'second' });
    }

    closeRegistrationModal() {
        this.registrationModal.hide();
        this.registrationModal = null;
    }

    onSubmit() {
        this.userService.createUser(this.model).subscribe(data => {
            this.errors.length = 0;
            this.closeRegistrationModal();
        }, errors => {
            this.errors = errors || [];
        });
    }
}
