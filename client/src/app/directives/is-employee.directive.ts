import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { SessionService } from '../services/session.service';

@Directive({
    selector: '[appIsEmployee]'
})
export class IsEmployeeDirective {

    constructor(
        private sessionService: SessionService,
        private templateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef
    ) { }

    @Input() set appIsEmployee(truthy: boolean) {
        const needsEmployee = truthy !== null ? truthy : true;
        if (this.sessionService.isEmployee() === needsEmployee) {
            this.viewContainer.createEmbeddedView(this.templateRef);
        } else {
            this.viewContainer.clear();
        }
    }
}
