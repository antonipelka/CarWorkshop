import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { UpdatePartDto } from '../dtos/parts/update-part.dto';
import { Part } from '../models/parts/part.entity';
import { CreatePartDto } from '../dtos/parts/create-part.dto';

@Injectable()
export class PartsService {

    constructor(private http: HttpService) { }

    getParts() {
        return this.http.get<Part[]>('/parts');
    }

    createPart(createPartDto: CreatePartDto) {
        return this.http.post<Part>('/parts', createPartDto);
    }

    deletePart(part: Part) {
        return this.http.delete(`/parts/${part.id}`);
    }

    updatePart(id: number, updatePartDto: UpdatePartDto) {
        return this.http.put<Part>(`/parts/${id}`, updatePartDto);
    }
}
