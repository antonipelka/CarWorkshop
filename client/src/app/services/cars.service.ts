import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Car } from 'app/models/cars/car.entity';
import { CreateCarDto } from 'app/dtos/cars/create-car.dto';
import { UpdateCarDto } from '../dtos/cars/update-car.dto';

@Injectable()
export class CarsService {

    constructor(private http: HttpService) { }

    getCars() {
        return this.http.get<Car[]>('/cars');
    }

    getCarsByClient(clientId: number) {
        return this.http.get<Car[]>(`/cars`, {
            params: {
                clientId
            }
        });
    }

    getCarsByOrderClient(orderId: number) {
        return this.http.get<Car[]>('/cars', {
            params: {
                orderId
            }
        });
    }

    createCar(createCarDto: CreateCarDto) {
        return this.http.post<Car>('/cars', createCarDto);
    }

    updateCar(id: number, updateCarDto: UpdateCarDto) {
        return this.http.put<Car>(`/cars/${id}`, updateCarDto);
    }

    deleteCar(car: Car) {
        return this.http.delete(`/cars/${car.id}`);
    }
}
