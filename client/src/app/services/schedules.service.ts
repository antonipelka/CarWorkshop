import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Schedule } from '../models/schedules/schedule.entity';
import { CreateScheduleDto } from 'app/dtos/schedules/create-schedule.dto';

@Injectable()
export class SchedulesService {

    constructor(private http: HttpService) { }

    getSchedules() {
        return this.http.get<Schedule[]>('/schedules');
    }

    createSchedule(createScheduleDto: CreateScheduleDto) {
        return this.http.post<Schedule>('/schedules', createScheduleDto);
    }

    deleteSchedule(schedule: Schedule){
        return this.http.delete(`/schedules/${schedule.id}`);
    }
}
