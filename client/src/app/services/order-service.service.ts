import { Injectable } from '@angular/core';
import { OrderService } from '../models/order-service/order-service.entity';
import { HttpService } from './http.service';
@Injectable()
export class OrderServiceService {

    constructor(private readonly http: HttpService) { }

    public changeOrderServiceCompletedState(id: number, isCompleted: boolean) {
        return this.http.post<OrderService>(`/orderServices/${id}`, { isCompleted });
    }

}
