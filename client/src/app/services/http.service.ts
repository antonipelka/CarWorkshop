import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { SessionService } from './session.service';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

@Injectable()
export class HttpService {
    constructor(private http: HttpClient, private sessionService: SessionService) { }

    baseUrl = environment.production ? '/api' : 'http://localhost:3000';

    private getOptions<T>(override?: { headers?: HttpHeaders, [key: string]: any }) {
        const options = {
            headers: new HttpHeaders()
        };

        const token = this.sessionService.getToken();
        if (token) {
            options.headers = options.headers.set('Authorization', `Bearer ${token}`);
        }

        return { ...options, ...override };
    }

    private getRealUrl(url) {
        return this.baseUrl + url;
    }

    public get<T>(url: string, overrideOptions?): Observable<T> {
        return this.http.get(this.getRealUrl(url), this.getOptions(overrideOptions)).catch(({error}) => {
            return Observable.throw(error.message);
        });
    }

    public post<T>(url: string, body: any, overrideOptions?): Observable<T> {
        return this.http.post(this.getRealUrl(url), body, this.getOptions(overrideOptions)).catch(({error}) => {
            return Observable.throw(error.message);
        });
    }

    public put<T>(url: string, body: any, overrideOptions?): Observable<T> {
        return this.http.put(this.getRealUrl(url), body, this.getOptions(overrideOptions)).catch(({error}) => {
            return Observable.throw(error.message);
        });
    }

    public delete(url: string, overrideOptions?): Observable<Object> {
        return this.http.delete(this.getRealUrl(url), this.getOptions(overrideOptions)).catch(({error}) => {
            return Observable.throw(error.message);
        });
    }

    public head<T>(url: string, overrideOptions?) {
        return this.http.head<T>(
            this.getRealUrl(url),
            {
                observe: 'response',
                responseType: 'json',
                ...this.getOptions(overrideOptions),
            }
        ).catch(({error}) => {
            return Observable.throw(error.message);
        });
    }
}
