import { Injectable } from '@angular/core';
import { CreateMessageDto } from 'app/dtos/messages/create-message.dto';
import { Message } from 'app/models/messages/message.entity';
import { HttpService } from './http.service';

@Injectable()
export class MessagesService {

    constructor(private http: HttpService) { }

    public getOrderMessages(id: number) {
        return this.http.get<Message[]>(`/orders/${id}/messages`);
    }

    public sendMessage(orderId: number, createMessageDto: CreateMessageDto) {
        return this.http.post<Message>(`/orders/${orderId}/messages`, createMessageDto);
    }
}
