import { Injectable } from '@angular/core';

@Injectable()
export class DateFormatter {

    constructor() { }

    public static formatDate(date: Date): String {
        let dd: String;
        let mm: String;
        let yyyy: String;

        const day = date.getDate();
        const month = date.getMonth() + 1;
        const year = date.getFullYear();

        dd = day.toString();
        mm = month.toString();
        yyyy = year.toString();

        if (day < 10) {
            dd = '0' + day;
        }
        if (month < 10) {
            mm = '0' + month;
        }

        return dd + '-' + mm + '-' + yyyy;
    }

    public static formatTime(date: Date): String {
        let mm: String;
        let hh: String;

        const minutes = date.getMinutes();
        const hours = date.getHours();

        mm = minutes.toString();
        hh = hours.toString();

        if (minutes < 10) {
            mm = '0' + minutes;
        }
        if (hours < 10) {
            hh = '0' + hours;
        }

        return hh + ':' + mm;
    }

}
