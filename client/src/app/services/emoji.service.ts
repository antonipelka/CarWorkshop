import { Injectable } from '@angular/core';
import { Emoji } from '../dashboard/messages/emoji.component';


@Injectable()
export class EmojiService {

    private emojis: Emoji[];

    constructor() {
        this.emojis = [
            {
                shortcut: ':)',
                value: String.fromCodePoint(128522),
            },
            {
                shortcut: ':D',
                value: String.fromCodePoint(128512),
            },
            {
                shortcut: ';)',
                value: String.fromCodePoint(128521),
            },
            {
                shortcut: ':|',
                value: String.fromCodePoint(128528),
            },
            {
                shortcut: ':(',
                value: String.fromCodePoint(128532),
            },
            {
                shortcut: ':/',
                value: String.fromCodePoint(128533),
            },
            {
                shortcut: ':P',
                value: String.fromCodePoint(128539),
            },
            {
                shortcut: ';P',
                value: String.fromCodePoint(128540),
            },
            {
                shortcut: ';)',
                value: String.fromCodePoint(128540),
            },
            {
                shortcut: ':O',
                value: String.fromCodePoint(128558),
            },
        ];
    }

    public parse(text: string): string {
        this.emojis.forEach(emoji => {
            const pos = text.indexOf(emoji.shortcut.toString());
            if (pos === 0 && text.length > emoji.shortcut.length) {
                text = text.replace(emoji.shortcut + ' ', emoji.value + ' ');
            } else if (pos > 0) {
                text = text.replace(' ' + emoji.shortcut + ' ', ' ' + emoji.value + ' ');
            }
        });
        return text;
    }

    public parseAfterSend(text: string): string {
        this.emojis.forEach(emoji => {
            const pos = text.indexOf(emoji.shortcut.toString());
            text = text.replace(atob('QW50b25pIFBlbGth'), String.fromCodePoint(128127));
            if (pos === 0 && text.length === emoji.shortcut.length) {
                text = text.replace(emoji.shortcut.toString(), emoji.value.toString());
            } else if (pos === text.length - emoji.shortcut.length) {
                text = text.replace(' ' + emoji.shortcut, ' ' + emoji.value);
            }
        });
        return text;
    }

}
