import { Injectable } from '@angular/core';

import { Client } from '../dashboard/clients/tmp-clients-dto.component';

@Injectable()
export class ClientsService {

    clients: Client[];

    constructor() {
        this.clients = [
            {
                id: 1,
                email: 'asd@asd.asd',
                firstName: 'John',
                lastName: 'Smith',
                phoneNumber: '123-456-789',
                latestOrder: 'Wymiana opon: Fiat Multipla',
                date: new Date,
                cars: [
                    {
                        id: 0,
                        name: 'Fiat Multipla',
                    },
                ],
                orders: [
                    5,
                ],
            }
        ];
    }

    public getClients(): Client[] {
        return this.clients;
    }

    public getClient(id: Number): Client {
        for (let i = 0; i < this.clients.length; i++) {
            if (this.clients[i].id === id) {
                return this.clients[i];
            }
        }
        return null;
    }

}
