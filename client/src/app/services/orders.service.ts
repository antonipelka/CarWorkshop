import { Injectable } from '@angular/core';
import { CreateOrderDto } from 'app/dtos/orders/create-order.dto';
import { UpdateOrderDto } from 'app/dtos/orders/update-order.dto';
import { ReservedTermsDto } from 'app/dtos/orders/reserved-terms.dto';
import { Order } from 'app/models/orders/order.entity';
import { HttpService } from './http.service';
import { NotificationsService } from './notifications.service';
import { OrderService } from 'app/models/order-service/order-service.entity';

@Injectable()
export class OrdersService {

    constructor(private notificationsService: NotificationsService, private http: HttpService) { }

    public getOrders() {
        return this.http.get<Order[]>('/orders');
    }

    public getClientOrders(id: number) {
        return this.http.get<Order[]>(`/orders/client/${id}`);
    }

    public getOrder(id: number) {
        return this.http.get<Order>(`/orders/${id}`);
    }

    public getOrderStatus(id: number) {
        return this.http.get<Order>(`/publicOrders/${id}`);
    }

    public getOrdersCount() {
        return this.http.head('/orders').map(response => {
            return +response.headers.get('X-Total-Count');
        });
    }

    public createOrder(createOrderDto: CreateOrderDto) {
        return this.http.post<Order>('/orders', createOrderDto);
    }

    public updateOrder(id: number, updateOrderDto: UpdateOrderDto) {
        return this.http.put<Order>(`/orders/${id}`, updateOrderDto);
    }

    public getReservedTerms() {
        return this.http.get<ReservedTermsDto>('/orders/terms');
    }

}
