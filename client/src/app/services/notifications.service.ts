import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Notification } from '../models/notifications/notification.entity';

@Injectable()
export class NotificationsService {
    constructor(private readonly http: HttpService) { }

    public getNotifications() {
        return this.http.get<Notification[]>('/notifications');
    }

    public getUnreadCount() {
        return this.http.head('/notifications').map(response => {
            return +response.headers.get('X-Total-Count');
        });
    }
}
