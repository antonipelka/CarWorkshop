import { Injectable } from '@angular/core';

import { OrderState } from './tmp-order-states-dto.component';

@Injectable()
export class OrderStatesService {

    orderStates: OrderState[];

    constructor() {
        this.orderStates = [
            {
                name: "Nie rozpoczęto naprawy.",
            },
            {
                name: "W trakcie naprawy.",
            },
            {
                name: "Zakończono!",
            },
        ]
    }

    public getOrderStates(): OrderState[] {
        return this.orderStates;
    }

}
