export class User {

    constructor(
        id: Number,
        isEmployee: Boolean
    ) {
        this.id = id;
        this.isEmployee = isEmployee;
    }

    public id: Number;
    public isEmployee: Boolean;
}