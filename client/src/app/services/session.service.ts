import { Injectable } from '@angular/core';
import { AuthenticatedUserDto } from 'app/dtos/auth/authenticated-user.dto';

@Injectable()
export class SessionService {
    private token: string;
    private user: AuthenticatedUserDto;

    public getToken() {
        return this.token || sessionStorage.getItem('token');
    }

    public setToken(token: string) {
        if (token != null) {
            sessionStorage.setItem('token', token);
        } else {
            sessionStorage.removeItem('token');
        }
        this.token = token;
    }

    public getUser() {
        return this.user;
    }

    public setUser(user: AuthenticatedUserDto) {
        this.user = user;
    }

    public isEmployee() {
        return this.user.isEmployee;
    }
}
