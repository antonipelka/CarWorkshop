import { Injectable } from '@angular/core';
import { CreateUserDto } from 'app/dtos/users/create-user.dto';
import { User } from 'app/models/users/user.entity';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { UpdateUserDto } from '../dtos/users/update-user.dto';
import { HttpService } from './http.service';

@Injectable()
export class UsersService {

    constructor(
        private http: HttpService
    ) { }

    public activate(code: string) {
        return this.http.post(`/users/activate/${code}`, {});
    }

    public getClients() {
        return this.http.get<User[]>('/users?type=client');
    }

    public getClient(id: number) {
        return this.http.get<User>(`/users/${id}`);
    }

    public getUserProfile() {
        return this.http.get<User>('/users/profile');
    }

    public createUser(createUserDto: CreateUserDto) {
        return this.http.post<User>('/users', createUserDto);
    }

    public updateUser(id: number, updateUserDto: UpdateUserDto) {
        return this.http.put<User>(`/users/${id}`, updateUserDto);
    }
}
