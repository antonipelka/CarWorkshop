import { Injectable } from '@angular/core';
import { CreateUserDto } from 'app/dtos/users/create-user.dto';
import { User } from 'app/models/users/user.entity';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpService } from './http.service';

@Injectable()
export class UserService {
    constructor(
        private http: HttpService
    ) { }

    public createUser(createUserDto: CreateUserDto): Observable<User> {
        return this.http.post('/users', createUserDto).catch(({error}) => {
            return Observable.throw(error.message);
        }) as Observable<User>;
    }
}
