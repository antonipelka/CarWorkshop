import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Service } from 'app/models/services/service.entity';
import { CreateServiceDto } from '../dtos/services/create-service.dto';
import { UpdateServiceDto } from '../dtos/services/update-service.dto';

@Injectable()
export class ServicesService {
    constructor(private http: HttpService) {
    }

    public getServices(allServices = false) {
        let url = '/services';
        if (allServices) { url += '?all=true'; }
        return this.http.get<Service[]>(url);
    }

    public getService(id: number) {
        return this.http.get<Service>(`/services/${id}`);
    }

    public createService(createServiceDto: CreateServiceDto) {
        return this.http.post<Service>('/services', createServiceDto);
    }

    public updateService(id: number, updateServiceDto: UpdateServiceDto) {
        return this.http.put<Service>(`/services/${id}`, updateServiceDto);
    }

    public deleteService(id: number) {
        return this.http.delete(`/services/${id}`);
    }
}
