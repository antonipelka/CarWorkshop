import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { GenerateTokenDto } from 'app/dtos/auth/generate-token.dto';
import { GeneratedTokenDto } from 'app/dtos/auth/generated-token.dto';
import { AuthenticatedUserDto } from 'app/dtos/auth/authenticated-user.dto';
import { HttpErrorResponse } from '../models/http-error-response';
import { SessionService } from './session.service';
import { HttpService } from './http.service';
import { Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import { ResetPasswordDto } from '../dtos/auth/reset-password.dto';

class SignInResult {
    success: boolean;
    errors?: any[];
}

@Injectable()
export class AuthenticationService {
    private loggedUserSubject = new Subject<AuthenticatedUserDto>();

    constructor(
        private http: HttpService,
        private sessionService: SessionService,
        private router: Router
    ) { }

    public preSignIn(): Observable<AuthenticatedUserDto> {
        const existingToken = this.sessionService.getToken();
        if (existingToken) {
            return this.setUserByToken(existingToken);
        }

        return Observable.of(null);
    }

    private setUserByToken(token) {
        return this.http.get('/auth/parseToken', {
            headers: new HttpHeaders({ 'Authorization': `Bearer ${token}` })
        }).map((user: AuthenticatedUserDto) => {
            this.sessionService.setUser(user);
            this.loggedUserSubject.next(user);
            return user;
        }).catch(() => {
            this.router.navigate(['/']);
            return Observable.of(null);
        });
    }

    public signIn(generateTokenDto: GenerateTokenDto): Promise<SignInResult> {
        return new Promise(resolve => {
            this.http.post('/auth/token', generateTokenDto).subscribe(({ token, user }: GeneratedTokenDto) => {
                this.sessionService.setToken(token);
                this.sessionService.setUser(user);
                this.loggedUserSubject.next(user);
                resolve({
                    success: true
                });
            }, (errors) => {
                this.sessionService.setToken(null);
                resolve({
                    success: false,
                    errors
                });
            });
        });
    }

    public logOut() {
        this.sessionService.setToken(null);
        this.router.navigate(['/']);
    }

    public isLoggedIn() {
        return this.sessionService.getToken() != null;
    }

    public getLoggedUser() {
        return this.sessionService.getUser();
    }

    public getLoggedUserSubject() {
        return this.loggedUserSubject;
    }

    public requestUser(fresh = false) {
        fresh ? this.preSignIn().subscribe() : this.loggedUserSubject.next(this.sessionService.getUser());
    }

    public resetPassword(email: string) {
        return this.http.post('/auth/resetPassword', { email });
    }

    public updatePassword(resetPassword: ResetPasswordDto) {
        return this.http.post('/auth/updatePassword', resetPassword);
    }
}
