import { AuthenticatedUserDto } from './authenticated-user.dto';

export class GeneratedTokenDto {
    expiresIn: number;
    token: string;
    user: AuthenticatedUserDto;
}
