export class ResetPasswordDto {
    code: string;
    password: string;
    passwordConfirmation: string;
}
