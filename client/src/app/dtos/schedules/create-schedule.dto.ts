export class CreateScheduleDto {
    weekDay: string;
    hour: number;
}
