export class UpdateScheduleDto {
    weekDay: string;
    hour: number;
}
