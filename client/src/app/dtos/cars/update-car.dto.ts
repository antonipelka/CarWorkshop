export class UpdateCarDto {
    name: string;

    constructor({ name }: UpdateCarDto) {
        Object.assign(this, { name });
    }
}
