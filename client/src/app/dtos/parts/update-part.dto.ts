export class UpdatePartDto {
    name: string;
    count: number;

    constructor({ name, count }: UpdatePartDto) {
        Object.assign(this, { name, count });
    }
}
