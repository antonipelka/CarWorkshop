export class UpdateUserDto {
    email = '';
    firstName = '';
    lastName = '';
    phoneNumber = '';
    isCompany = false;
    companyName = '';
    identificationNumber = '';
    address = '';
    isEmployee = false;

    constructor({
        email,
        firstName,
        lastName,
        address,
        companyName,
        isCompany,
        identificationNumber,
        phoneNumber,
        isEmployee,
    }: UpdateUserDto) {
        Object.assign(this, {
            email,
            firstName,
            lastName,
            address,
            companyName,
            isCompany,
            identificationNumber,
            phoneNumber,
            isEmployee,
        });
    }
}
