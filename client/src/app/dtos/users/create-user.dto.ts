export class CreateUserDto {
    readonly email: string = '';
    readonly password: string = '';
    readonly passwordConfirmation: string = '';
    readonly firstName: string = '';
    readonly lastName: string = '';
    readonly phoneNumber: string = '';
    readonly isCompany: boolean = false;
    readonly companyName: string = '';
    readonly identificationNumber: string = '';
    readonly address: string = '';
    readonly rulesAgreement: boolean = false;
}
