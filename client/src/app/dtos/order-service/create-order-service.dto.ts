export class CreateOrderServiceDto {
    orderId: number;
    serviceId: number;
    isCompleted: boolean;
}
