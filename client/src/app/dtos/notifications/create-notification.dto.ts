export class CreateNotificationDto {
    message: string;
    readonly createdAt: Date;
    userId: number;
    orderId: number;
}
