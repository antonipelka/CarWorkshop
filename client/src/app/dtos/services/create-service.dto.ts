export class CreateServiceDto {
    name: string;
    price = 0;
    description = '';
    isChoosable = false;
    partIds: number[] = [];
}
