export class UpdateServiceDto {
    name: string;
    price: number;
    description: string;
    isChoosable: boolean;
    partIds: number[];

    constructor({
        name,
        price,
        description,
        isChoosable,
        partIds,
    }: UpdateServiceDto) {
        Object.assign(this, {
            name,
            price,
            description,
            isChoosable,
            partIds,
        });
    }
}
