import { Order } from '../../models/orders/order.entity';

export class UpdateOrderDto {
    term: Date;
    comment: string;
    state: string;
    carId?: number;
    employeeId?: number;
    serviceIds?: number[];

    constructor({
        term,
        car,
        comment,
        state,
        orderServices,
    }: Order) {
        Object.assign(this, {
            term,
            carId: car.id,
            comment,
            serviceIds: orderServices.map(orderService => orderService.service.id),
            state,
        });
    }
}
