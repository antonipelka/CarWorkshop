export class CreateOrderDto {
    term: Date;
    comment = '';
    carId: number;
    serviceIds: number [] = [];
}
