import { Order } from 'app/models/orders/order.entity';
import { Schedule } from 'app/models/schedules/schedule.entity';

export class ReservedTermsDto {
    terms: Partial<Order>[];
    schedules: Schedule[];

    constructor(object?: {
        terms: Partial<Order>[],
        schedules: Schedule[],
    }) {
        Object.assign(this, object);
    }
}
