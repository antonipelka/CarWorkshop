import { Component, OnInit } from '@angular/core';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { SessionService } from '../session.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit{

  constructor(private sessionService: SessionService) { 
  }

  ngOnInit() {
  }

}