import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersWorkerComponent } from './orders-worker.component';

describe('OrdersWorkerComponent', () => {
  let component: OrdersWorkerComponent;
  let fixture: ComponentFixture<OrdersWorkerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersWorkerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersWorkerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
