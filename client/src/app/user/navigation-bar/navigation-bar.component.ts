import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { TabDirective } from 'ngx-bootstrap/tabs';
import { Router } from '@angular/router';
import { SessionService, UserType } from '../../session.service';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NavigationBarComponent implements OnInit {

  uType = UserType;

  userType: UserType = UserType.worker;
  public userTypeString: string = 'worker';

  clientsNumber: Number = 3;
  workerOrdersNumber: Number = 4;
  clientOrdersNumber: Number = 5;
  servicesNumber: Number = 10;
  workerNotificationsNumber: Number = 32;
  clientNotificationsNumber: Number = 12;

  constructor(private router: Router, private sessionService: SessionService) {
  }

  ngOnInit() {
    this.userType = this.sessionService.getUserType();
  }

  logout(data: TabDirective) {
    // Wylogowanie
    this.sessionService.signOut();
    this.router.navigateByUrl('');
  }

}
